# Graph2svg

This is a library to facilitate the drawing of schemas of simple graphs (nodes, edges, etc.) using csv as a standard output format supported by latex, typst or even libreoffice/word wysiwyg.

## Requirements

You should install the [z3 solver](https://github.com/Z3Prover/z3) from Microsoft.
The [rust](https://www.rust-lang.org/learn/get-started) toolchain including the compiler rustc and the cargo build system.
Also there is some requirement to the fontconfig library.

### On debian/ubuntu based distros

```bash
# rustup to install rust tool chain
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# install some system requirement for managing font
sudo apt install z3 libfontconfig1-dev font-jetbrains-mono
```

### On arch based distros

```bash
sudo pacman -S z3 fontconfig ttf-jetbrains-mono
```

## Features

- Hierachical nesting of nodes.
- Array2D quick and easy (with optional margin between cells).
- Automatically place node (using the z3 solver).
- Add constraints to the placement:
  - Align nodes (optional anchor, default center).
  - Order left to right or top to bottom of nodes.
  - Align + Order in one constraint.
  - Alignement with respect to anchor on a segment between 2 nodes.
- Automatically connect some nodes using arrow or edge.
- Label on arrow.
- Vertical Text.

## Documentation:

The API is documented with rustdoc, you can generate the html doc and open it in your default browser with:

```bash
cargo doc --open
```

### Troubleshootings

The default font is JetBrains Mono you can install it using your package manager or manually. You can change it by any font you like. Just be aware that the name of the font should match exactly the output of `fc-list` to be garantied that `fc-match` will find the correct font.

## Todos:

- Test more use cases.
- Improve error handling and error messages.
