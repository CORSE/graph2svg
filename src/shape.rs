use crate::helpers::{Vec2, Viewbox};
use crate::node::{NodeID, DEFAULT_ROUNDED};
use crate::style::{Styler, DEFAULT_CIRCLE, DEFAULT_RECT};
use svg::node::element::{Circle, Group, Rectangle};

/// Shape for rendering nodes.
#[derive(Debug, Clone)]
pub enum Shape {
    /// `<circle>` svg primitive to generate.
    Circle,
    /// Rounded corners for `<rect rx=?, ry=?>` values.
    Rectangle((u32, u32)),
    /// No shape to draw.
    None,
}

/// Default rectangle [Shape] with rounded corners.
pub fn shape_rect_rounded() -> Shape {
    Shape::Rectangle(DEFAULT_ROUNDED)
}

/// Default rectangle [Shape] with sharp corners.
pub fn shape_rect_sharp() -> Shape {
    Shape::Rectangle((0, 0))
}

pub(crate) trait ExtentSvgGroup {
    fn add_shape(self, node_id: NodeID, shape: Shape, viewbox: &Viewbox, styler: &Styler) -> Self;
}

impl ExtentSvgGroup for Group {
    fn add_shape(self, node_id: NodeID, shape: Shape, viewbox: &Viewbox, styler: &Styler) -> Self {
        type S = Shape;
        let css_class = styler
            .node_styles
            .get(&node_id)
            .map(|(shape, _)| shape.as_str());
        match shape {
            S::None => self,
            S::Circle => {
                let center: Vec2 = viewbox.topleft + (viewbox.dimensions / 2);
                let circle = Circle::new()
                    .set("cx", center.x)
                    .set("cy", center.y)
                    .set("r", viewbox.dimensions.x / 2)
                    .set("class", css_class.unwrap_or(DEFAULT_CIRCLE));
                self.add(circle)
            }
            S::Rectangle(rounded, ..) => {
                let rect = Rectangle::new()
                    .set("x", viewbox.topleft.x)
                    .set("y", viewbox.topleft.y)
                    .set("width", viewbox.dimensions.x)
                    .set("height", viewbox.dimensions.y)
                    .set("rx", rounded.0)
                    .set("ry", rounded.1)
                    .set("class", css_class.unwrap_or(DEFAULT_RECT));
                self.add(rect)
            }
        }
    }
}
