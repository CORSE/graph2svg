use std::collections::{HashMap, HashSet};
use svg::node::element::Group;

use crate::{
    constraints::{Constraint, DimsConstraint},
    edge::Edge,
    error::Graph2SvgErr,
    helpers::{svg_text, text_dimensions, Vec2, Viewbox, DOWN, RIGHT},
    shape::{ExtentSvgGroup, Shape},
    solver::{NodesSolverInfo, SolverZ3},
    style::Styler,
};

pub type NodeID = usize;
pub(crate) type NodesPlacements = HashMap<NodeID, Viewbox>;

pub(crate) const DEFAULT_MARGIN: Vec2 = Vec2 { x: 10, y: 10 };
pub const DEFAULT_ROUNDED: (u32, u32) = (5, 5);

/// Main recursive struct to store informations about node, how to render it with some styling
/// arguments and also geometric informations like shape, topleft and dimensions.
#[derive(Debug, Clone)]
pub struct Node {
    id: NodeID,
    viewbox: Viewbox,
    content: NodeContent,
    draw_shape: Shape,
    margin: Vec2,
}

impl Node {
    #[inline]
    pub fn id(&self) -> NodeID {
        self.id
    }

    #[inline]
    pub fn shape(&self) -> &Shape {
        &self.draw_shape
    }

    pub fn with_shape(mut self, shape: Shape) -> Self {
        self.draw_shape = shape;
        self
    }

    #[inline]
    pub fn margin(&self) -> (i32, i32) {
        (self.margin.x, self.margin.y)
    }

    #[inline]
    pub(crate) fn content(&self) -> &NodeContent {
        &self.content
    }

    pub fn new(id: NodeID, content: NodeContent, draw_shape: Shape) -> Self {
        Self {
            id,
            content,
            viewbox: Viewbox::default(),
            margin: DEFAULT_MARGIN,
            draw_shape,
        }
    }

    pub fn with_margin(mut self, x: u32, y: u32) -> Self {
        self.margin.x = x as i32;
        self.margin.y = y as i32;
        self
    }

    pub(crate) fn get_viewboxes(&self, boxes: &mut NodesPlacements) {
        type N = NodeContent;
        boxes.insert(self.id, self.viewbox);
        match &self.content {
            N::Container(children, ..) => {
                children.iter().for_each(|child| child.get_viewboxes(boxes));
            }
            N::Array2D(cells, ..) => {
                cells
                    .iter()
                    .flat_map(|line| line.iter())
                    .for_each(|cell| cell.get_viewboxes(boxes));
            }
            _ => (),
        }
    }

    fn contains(&self, node: NodeID) -> bool {
        if self.id == node {
            return true;
        }
        type N = NodeContent;
        match &self.content {
            N::Container(children, ..) => children.iter().any(|c| c.contains(node)),
            N::Array2D(cells, ..) => cells
                .iter()
                .flat_map(|line| line.iter())
                .any(|c| c.contains(node)),
            _ => false,
        }
    }

    pub(crate) fn infer_dimensions(
        &mut self,
        styler: &mut Styler,
        constraints: &[Constraint],
        edges: &[Edge],
    ) -> Result<Vec2, Graph2SvgErr> {
        type N = NodeContent;
        let dims = match &mut self.content {
            // base cases
            N::Text(text, is_vertical) => {
                let style = styler.node_text_style(self.id)?;
                text_dimensions(text, style.clone(), &mut styler.fonts_cache, *is_vertical)?
            }

            // recurse + infer the width and height based on array layout and cell dimensions
            N::Array2D(cells, margin_inter_cell) => {
                // find the biggest individual cell + the biggest number of columns to get the viewbox
                let margin_inter_cell: Vec2 = margin_inter_cell.into();
                let mut max_cell_dim = Vec2::zero();
                let mut max_ncols = 0;
                for line in cells.iter_mut() {
                    if max_ncols < line.len() {
                        max_ncols = line.len();
                    }
                    for cell in line.iter_mut() {
                        let dim = cell.infer_dimensions(styler, constraints, edges)?;
                        if dim.x > max_cell_dim.x {
                            max_cell_dim.x = dim.x;
                        }
                        if dim.y > max_cell_dim.y {
                            max_cell_dim.y = dim.y;
                        }
                    }
                }
                // uniformize the sizes of each cell of the rectangle
                cells.iter_mut().for_each(|line| {
                    line.iter_mut().for_each(|cell| {
                        cell.viewbox.dimensions = max_cell_dim;
                    })
                });
                // compute the overall size
                max_cell_dim += margin_inter_cell;
                Vec2 {
                    x: max_ncols as i32 * max_cell_dim.x,
                    y: cells.len() as i32 * max_cell_dim.y,
                }
            }

            // recurse + maintain an over approximation of the width and height
            N::Container(children, safety_dist) => {
                assert!(!children.is_empty());
                let label_size =
                    labels_max_size(children, edges, styler)? + DEFAULT_MARGIN.x as usize;
                *safety_dist = label_size.max(*safety_dist);

                let dims: Result<Vec<Vec2>, Graph2SvgErr> = children
                    .iter_mut()
                    .map(|child| child.infer_dimensions(styler, constraints, edges))
                    .collect();
                container_infer_dim(children, *safety_dist, dims?, constraints)
            }
        };
        // round the dimensions to a multiple of 2
        let dimensions: Vec2 = (((dims + 1) / 2) + self.margin) * 2;
        let dimensions = if let Shape::Circle = self.draw_shape {
            dimensions.maximised()
        } else {
            dimensions
        };
        self.viewbox = Viewbox {
            topleft: Vec2::zero(),
            dimensions,
        };
        println!(
            "Node {} width = {} height = {}",
            self.id, dimensions.x, dimensions.y
        );
        Ok(dimensions)
    }

    pub(crate) fn root_solve_positions(
        &mut self,
        constraints: Vec<Constraint>,
        edges: &[Edge],
        local_safety: HashMap<(NodeID, NodeID), usize>,
        dims_constr: Vec<DimsConstraint>,
    ) -> Result<(), Graph2SvgErr> {
        type C = NodeContent;
        let mut infos: HashMap<NodeID, NodesSolverInfo> = HashMap::new();
        let (children, safety_dist) = match &mut self.content {
            C::Container(children, safety_dist, ..) => (children, *safety_dist),
            _ => panic!("method supposed to be used only on root"),
        };
        children
            .iter()
            .for_each(|c| c.fill_info_solver(&mut infos, &mut vec![], safety_dist));
        let bad_id = |cst: &DimsConstraint, n| Graph2SvgErr::BadIDInDimsConstraint(cst.clone(), n);
        for constr_d in dims_constr {
            let (n1, n2) = constr_d.get_nodes();
            let dims1 = infos
                .get(&n1)
                .ok_or_else(|| bad_id(&constr_d, n1))?
                .dimensions;
            let dims2 = infos
                .get(&n2)
                .ok_or_else(|| bad_id(&constr_d, n2))?
                .dimensions;
            let (dims1, dims2) = constr_d.get_dims(dims1, dims2);
            infos.get_mut(&n1).unwrap().dimensions = dims1;
            infos.get_mut(&n2).unwrap().dimensions = dims2;
            children.iter_mut().for_each(|c| c.set_dims(n1, dims1));
            children.iter_mut().for_each(|c| c.set_dims(n2, dims2));
        }
        let positions = SolverZ3::new().solve_positions(
            &constraints,
            edges,
            infos,
            self.viewbox.dimensions,
            self.margin,
            local_safety,
        )?;
        children
            .iter_mut()
            .for_each(|c| c.set_solver_positions(&positions));

        Ok(())
    }

    fn set_dims(&mut self, id: NodeID, dims: Vec2) {
        type C = NodeContent;
        match &mut self.content {
            C::Text(..) | C::Container(..) if id == self.id => {
                self.viewbox.dimensions = dims;
            }
            C::Array2D(cells, margin) if id == self.id => {
                // resize array means to resize every cells to match the given size
                let margin = Vec2::from(margin);
                let n_rows = cells.len();
                let n_cols = cells.iter().map(|v| v.len()).max().unwrap();
                // tricky here it is reversed since n_rows impact the height, y axis
                let n_dims = Vec2::from((n_cols, n_rows));
                // this can actually be less than the expected size
                // the worst case is n_row pixels for y and n_col pixels for x
                let total_cells_dims = dims - (self.margin * 2i32) - (n_dims - 1i32) * margin;
                let cell_dims = total_cells_dims / n_dims;
                let pixel_rest = total_cells_dims - n_dims * cell_dims;

                cells
                    .iter_mut()
                    .flat_map(|line| line.iter_mut())
                    .for_each(|c| c.set_dims(c.id, cell_dims));

                if pixel_rest.x != 0 {
                    let cell_dims = cell_dims + RIGHT * pixel_rest;
                    // resize last column
                    cells.iter_mut().for_each(|line| {
                        let c = line.last_mut().unwrap();
                        c.set_dims(c.id, cell_dims)
                    })
                }

                if pixel_rest.y != 0 {
                    let cell_dims = cell_dims + DOWN * pixel_rest;
                    // resize last row
                    cells
                        .last_mut()
                        .unwrap()
                        .iter_mut()
                        .for_each(|c| c.set_dims(c.id, cell_dims))
                }

                self.viewbox.dimensions = dims;
            }
            C::Container(children, ..) => children.iter_mut().for_each(|c| c.set_dims(id, dims)),
            C::Array2D(cells, ..) => cells
                .iter_mut()
                .flat_map(|line| line.iter_mut())
                .for_each(|c| c.set_dims(id, dims)),
            _ => (),
        }
    }

    #[inline]
    pub(crate) fn viewbox(&self) -> Viewbox {
        self.viewbox
    }

    pub(crate) fn svg_group(self, styler: &Styler) -> Group {
        type C = NodeContent;
        let group = Group::new().add_shape(self.id, self.draw_shape, &self.viewbox, styler);
        match self.content {
            C::Text(text, is_vertical) => {
                let offset: Vec2 = self.viewbox.dimensions / 2;
                let css_class = styler.node_text_class(self.id);
                let position = self.viewbox.topleft + offset;

                group.add(svg_text(&text, css_class, is_vertical, position))
            }
            C::Array2D(cells, ..) => cells
                .into_iter()
                .flat_map(|line| line.into_iter())
                .fold(group, |acc, cell| acc.add(cell.svg_group(styler))),
            C::Container(children, ..) => children
                .into_iter()
                .fold(group, |acc, child| acc.add(child.svg_group(styler))),
        }
    }

    pub(crate) fn get_all_ids(&self, ids: &mut HashSet<NodeID>) {
        ids.insert(self.id);
        type C = NodeContent;
        match &self.content {
            C::Array2D(cells, ..) => cells
                .iter()
                .flat_map(|line| line.iter())
                .for_each(|cell| cell.get_all_ids(ids)),

            C::Container(children, ..) => children.iter().for_each(|cell| cell.get_all_ids(ids)),
            _ => (),
        }
    }
    fn fill_info_solver(
        &self,
        infos: &mut HashMap<NodeID, NodesSolverInfo>,
        parents: &mut Vec<NodeID>,
        parent_dist: usize,
    ) {
        type C = NodeContent;

        let (is_leaf, safety_dist) = match &self.content {
            C::Container(children, safety_dist, ..) => {
                parents.push(self.id);
                children
                    .iter()
                    .for_each(|c| c.fill_info_solver(infos, parents, *safety_dist));
                parents.pop();
                (false, *safety_dist)
            }
            _ => (true, parent_dist),
        };
        infos.insert(
            self.id,
            NodesSolverInfo {
                dimensions: self.viewbox.dimensions,
                margin: self.margin,
                parents: parents.clone(),
                is_leaf,
                safety_dist,
            },
        );
    }

    fn set_solver_positions(&mut self, positions: &HashMap<NodeID, Vec2>) {
        type C = NodeContent;
        self.viewbox.topleft = *positions.get(&self.id).unwrap();
        println!(
            "Node {} x = {} y = {}",
            self.id, self.viewbox.topleft.x, self.viewbox.topleft.y,
        );
        match &mut self.content {
            C::Array2D(cells, margin) => {
                let margin: Vec2 = margin.into();
                let offset = self.viewbox.topleft + self.margin;
                let cell_dim = cells
                    .iter()
                    .flat_map(|line| line.iter().map(|cell| cell.viewbox.dimensions))
                    .next()
                    .unwrap_or_default();

                for (i, line) in cells.iter_mut().enumerate() {
                    let y = i as i32;
                    for (j, cell) in line.iter_mut().enumerate() {
                        let rel_pos = Vec2 { x: j as i32, y };
                        cell.viewbox.topleft = offset + rel_pos * (cell_dim + margin);
                    }
                }
            }
            C::Container(children, ..) => children
                .iter_mut()
                .for_each(|c| c.set_solver_positions(positions)),
            _ => (),
        }
    }
    pub(crate) fn sanity_check(&self) -> Result<(), Graph2SvgErr> {
        type C = NodeContent;
        match &self.content {
            C::Array2D(cells, ..) => {
                if cells.is_empty() || cells.iter().any(Vec::is_empty) {
                    return Err(Graph2SvgErr::EmptyNode(self.id));
                }
                for cell in cells.iter().flat_map(|v| v.iter()) {
                    cell.sanity_check()?;
                }
            }
            C::Container(children, ..) => {
                if children.is_empty() {
                    return Err(Graph2SvgErr::EmptyNode(self.id));
                }
                for child in children.iter() {
                    child.sanity_check()?;
                }
            }
            _ => (),
        };
        Ok(())
    }
}

fn labels_max_size(
    children: &[Node],
    edges: &[Edge],
    styler: &mut Styler,
) -> Result<usize, Graph2SvgErr> {
    let mut res = 0;
    for e in edges.iter().filter(|e| {
        children
            .iter()
            .any(|c| c.contains(e.from_node()) || c.contains(e.to_node()))
    }) {
        if let Some(label) = e.get_label() {
            let size = text_dimensions(
                label,
                styler.edge_text_style(e.from_node(), e.to_node())?.clone(),
                &mut styler.fonts_cache,
                e.label_vertical,
            )?;
            let max_size = size.x.max(size.y) as usize;
            res = res.max(max_size);
        }
    }
    Ok(res)
}

/// TODO: find a way to refactor all of the safety stuff.
/// It should only be configured with the default value or the local_safety dict.
/// The informations passed to solver are really weirds and all should be using the same
/// parameters.
fn container_infer_dim(
    children: &[Node],
    safety_dist: usize,
    mut children_d: Vec<Vec2>,
    constraints: &[Constraint],
) -> Vec2 {
    let safety_dist = safety_dist as i32;
    let mut safety_x: Vec<i32> = children.iter().map(|_| safety_dist).collect();
    let mut safety_y: Vec<i32> = children.iter().map(|_| safety_dist).collect();
    constraints
        .iter()
        .filter_map(|c| {
            let involved: Vec<NodeID> = children
                .iter()
                .enumerate()
                .filter_map(|(i, child)| c.contains(&child.id).then_some(i))
                .collect();
            (!involved.is_empty()).then_some((c, involved))
        })
        .for_each(|(c, nodes)| {
            if c.is_align_v() {
                let i_max_x = *nodes.iter().max_by_key(|&&i| children_d[i].x).unwrap();
                nodes.into_iter().filter(|&i| i != i_max_x).for_each(|i| {
                    children_d[i].x = 0;
                    safety_x[i] = 0;
                });
            } else if c.is_align_h() {
                let i_max_y = *nodes.iter().max_by_key(|&&i| children_d[i].y).unwrap();
                nodes.into_iter().filter(|&i| i != i_max_y).for_each(|i| {
                    children_d[i].y = 0;
                    safety_y[i] = 0;
                });
            }
        });

    let safety = Vec2 {
        x: safety_x.into_iter().sum(),
        y: safety_y.into_iter().sum(),
    } - safety_dist;

    children_d.into_iter().sum::<Vec2>() + safety
}

/// Recursive content of a [Node] to allow nesting.
#[derive(Debug, Clone)]
pub enum NodeContent {
    /// Leaf node with text inside.
    Text(String, bool),
    /// 2D array with inner margins between each cell
    Array2D(Vec<Vec<Node>>, (u32, u32)),
    /// Generic nesting primitive.
    Container(Vec<Node>, usize),
}

/// Create an array [Node] filled with cells containing text. The default array style for cells is using
/// sharp corners which difer from default `Text` node style with rounded corners.
/// # Warning
/// The nodes created will be using ids from id_base+1 to id_base + n_cells.
/// So be careful with id of the other nodes of your graph, and avoid conflicting ids.
pub fn array2d<T: ToString>(id_base: NodeID, content: Vec<Vec<T>>) -> Node {
    let mut idxs = id_base + 1;
    let cells: Vec<Vec<Node>> = content
        .into_iter()
        .map(|line| {
            line.into_iter()
                .map(|t| {
                    let id = idxs;
                    idxs += 1;
                    let content = NodeContent::Text(t.to_string(), false);
                    Node::new(id, content, Shape::Rectangle((0, 0)))
                })
                .collect()
        })
        .collect();
    Node::new(id_base, NodeContent::Array2D(cells, (0, 0)), Shape::None)
}

/// Same as [array2d] but with additionnal parameter to set inner margin between each cell.
pub fn array2d_cell_margin<T: ToString>(
    id_base: NodeID,
    content: Vec<Vec<T>>,
    margin_x: u32,
    margin_y: u32,
) -> Node {
    let mut array = array2d(id_base, content);
    if let NodeContent::Array2D(_, margin) = &mut array.content {
        margin.0 = margin_x;
        margin.1 = margin_y;
    }
    array
}

/// Helper function to create a leaf [Node] containing some text using default styling (rectangle
/// with rounded corners + default text).
pub fn text_boxed<T: ToString>(id: NodeID, text: T) -> Node {
    Node::new(
        id,
        NodeContent::Text(text.to_string(), false),
        Shape::Rectangle(DEFAULT_ROUNDED),
    )
}

/// Helper function to create a leaf [Node] containing some text using default styling (rectangle
/// with rounded corners + default text).
pub fn vertical_text_boxed<T: ToString>(id: NodeID, text: T) -> Node {
    Node::new(
        id,
        NodeContent::Text(text.to_string(), true),
        Shape::Rectangle(DEFAULT_ROUNDED),
    )
}

/// Helper function to create a leaf [Node] containing some text using circle shape instead of the default rectangle with rounded corner.
pub fn text_circled<T: ToString>(id: NodeID, text: T) -> Node {
    Node::new(
        id,
        NodeContent::Text(text.to_string(), false),
        Shape::Circle,
    )
}

#[cfg(test)]
mod test_node {
    use super::*;

    #[test]
    fn test_dims_base() {
        let mut node = text_boxed(0, "test");
        let dims = node.infer_dimensions(&mut Styler::new(), &[], &[]).unwrap();
        assert!(dims.is_bigger(DEFAULT_MARGIN));
    }

    const N: usize = 4;
    #[test]
    fn test_dims_array() {
        let id_base = 0;
        let content = vec![vec!["test"; N]; N];
        let mut array = array2d(id_base, content);
        let dim_array = array
            .infer_dimensions(&mut Styler::new(), &[], &[])
            .unwrap();

        let cell_dim = if let NodeContent::Array2D(array2d, ..) = array.content {
            array2d[0][0].viewbox.dimensions
        } else {
            panic!("WTF")
        };
        let margin = DEFAULT_MARGIN * 2;
        assert_eq!(
            dim_array,
            margin + (cell_dim * N),
            "Array2D NxN should have dim == max_cell={cell_dim:?} * N={N} + margin={margin:?}",
        );
    }
}
