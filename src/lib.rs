//! A library to quickly create nice looking svg for graph based schema or drawing.
//!
//! # Example
//! ```rust
//! use graph2svg::prelude::*;
//!
//! Graph::new(vec![
//!   text_boxed(1, "Hello World!"),
//!   text_boxed(2, "Hi"),
//!   text_boxed(3, "So long"),
//!   text_boxed(4, "Bye Bye"),
//! ])
//! .add_constraints(vec![
//!        align_left_to_right(vec![1, 2]),
//!        align_top_to_bottom(vec![3, 4]),
//!        align_vertical_segment(vec![3, 4], (1, 2)),
//!        align_horizontal_segment(vec![1, 2], (3, 4)),
//! ])
//!   .add_edges(vec![edge(1, 2), edge(3, 4)])
//!   .set_theme(ColorTheme::CatppuccinMocha)
//!   .to_svg("/tmp/test.svg")
//! ```
pub mod constraints;
pub mod edge;
pub mod error;
mod graph;
mod helpers;
pub mod node;
pub mod shape;
mod solver;
pub mod style;

pub use constraints::Constraint;
pub use edge::{arrow, edge, Edge};
pub use graph::{Graph, DEFAULT_SAFETY_DIST};
pub use node::{array2d, text_boxed, Node};
pub use shape::Shape;

pub mod prelude {
    pub use super::*;
    pub use crate::constraints::*;
    pub use crate::edge::*;
    pub use crate::node::*;
    pub use crate::shape::*;
    pub use style::ColorTheme;
}
