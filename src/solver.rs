use crate::{
    constraints::{Anchor, Constraint},
    edge::Edge,
    error::Graph2SvgErr,
    helpers::Vec2,
    node::NodeID,
};
use std::collections::HashMap;
use z3::{
    ast::{Ast, Bool, Int},
    Config, Context, Optimize, SatResult,
};

const X_AXIS: usize = 0;
const Y_AXIS: usize = 1;
const TIMEOUT_MS: u64 = 1000;

pub struct SolverZ3 {
    context: Context,
}

struct SolverParams<'c> {
    ctx: &'c Context,
    constraints: Vec<Bool<'c>>,
    variables: HashMap<NodeID, [Int<'c>; 2]>,
}

impl<'c> SolverParams<'c> {
    fn new(ctx: &'c Context, nodes: &HashMap<NodeID, NodesSolverInfo>) -> Self {
        Self {
            constraints: vec![],
            variables: nodes
                .keys()
                .map(|&id| {
                    (
                        id,
                        [
                            Int::new_const(ctx, format!("x_{id}")),
                            Int::new_const(ctx, format!("y_{id}")),
                        ],
                    )
                })
                .collect(),
            ctx,
        }
    }
    fn set_constraints(
        mut self,
        constraints: &[Constraint],
        boxes: &HashMap<NodeID, NodesSolverInfo>,
        local_dists: &HashMap<(NodeID, NodeID), usize>,
    ) -> Result<Self, Graph2SvgErr> {
        type C = Constraint;
        for constr in constraints.iter() {
            constr.check(constraints)?;
            match constr {
                C::AlignHorizontal(nodes, anchor) => {
                    self.set_align(nodes, anchor, boxes, Y_AXIS)?
                }
                C::LeftToRight(nodes) => self.set_order(nodes, boxes, local_dists, X_AXIS)?,
                C::AlignLeftToRight(nodes, anchor) => {
                    self.set_align(nodes, anchor, boxes, Y_AXIS)?;
                    self.set_order(nodes, boxes, local_dists, X_AXIS)?
                }
                C::AlignHorizontalSegment(nodes, anchor, seg, anchor_seg) => {
                    self.set_align_seg(nodes, anchor, boxes, Y_AXIS, seg, anchor_seg)?
                }
                C::AlignVertical(nodes, anchor) => self.set_align(nodes, anchor, boxes, X_AXIS)?,
                C::TopToBottom(nodes) => self.set_order(nodes, boxes, local_dists, Y_AXIS)?,
                C::AlignTopToBottom(nodes, anchor) => {
                    self.set_align(nodes, anchor, boxes, X_AXIS)?;
                    self.set_order(nodes, boxes, local_dists, Y_AXIS)?
                }
                C::AlignVerticalSegment(nodes, anchor, seg, anchor_seg) => {
                    self.set_align_seg(nodes, anchor, boxes, X_AXIS, seg, anchor_seg)?
                }
            }
        }
        Ok(self)
    }

    fn set_align(
        &mut self,
        nodes: &[NodeID],
        anchor: &Anchor,
        boxes: &HashMap<NodeID, NodesSolverInfo>,
        axis: usize,
    ) -> Result<(), Graph2SvgErr> {
        let bad_id = |id| {
            let constr = if axis == X_AXIS {
                Constraint::AlignVertical(nodes.to_vec(), *anchor)
            } else {
                Constraint::AlignHorizontal(nodes.to_vec(), *anchor)
            };
            Graph2SvgErr::BadIDInConstraint(constr.to_string(), id)
        };
        let anchor = anchor.to_float()?;
        for pair in nodes.windows(2) {
            let n1 = &pair[0];
            let n2 = &pair[1];
            // get z3 variables corresponding to x or y for both nodes
            let var1 = &self.variables.get(n1).ok_or_else(|| bad_id(*n1))?[axis];
            let var2 = &self.variables.get(n2).ok_or_else(|| bad_id(*n2))?[axis];
            // get the constant width or height
            let size1 = boxes.get(n1).unwrap().dimensions[axis] as f32;
            let size2 = boxes.get(n2).unwrap().dimensions[axis] as f32;
            // constraint that x1 - x2 == (w1 - w2) / 2
            let dif = var1 - var2;
            let dif_size = anchor * (size2 - size1); // inverted order 1 and 2
            let dif_size = Int::from_i64(self.ctx, dif_size.round() as i64);
            let z3_cst = dif._eq(&dif_size);
            // println!("align = {z3_cst:?}");
            self.constraints.push(z3_cst);
        }
        Ok(())
    }

    fn set_align_seg(
        &mut self,
        nodes: &[NodeID],
        anchor: &Anchor,
        infos: &HashMap<NodeID, NodesSolverInfo>,
        axis: usize,
        seg: &(NodeID, NodeID),
        anchor_seg: &Anchor,
    ) -> Result<(), Graph2SvgErr> {
        let bad_id = |id| {
            let constr = if axis == X_AXIS {
                Constraint::AlignVertical(nodes.to_vec(), *anchor)
            } else {
                Constraint::AlignHorizontal(nodes.to_vec(), *anchor)
            };
            Graph2SvgErr::BadIDInConstraint(constr.to_string(), id)
        };
        let anchor = anchor.to_float()?;
        let anchor_seg = anchor_seg.to_float()?;
        let anchor_seg = if anchor_seg != 0.0 {
            (1.0 / anchor_seg).round() as u64
        } else {
            u64::MAX
        };
        let anchor_seg = Int::from_u64(self.ctx, anchor_seg);
        // we assume at this point that seg.0 is the min x or y
        let seg_min = &self.variables.get(&seg.0).ok_or_else(|| bad_id(seg.0))?[axis];
        let tmp_size = infos.get(&seg.0).unwrap().dimensions[axis];
        let seg_min_size = Int::from_u64(self.ctx, tmp_size as u64);
        let seg_max = &self.variables.get(&seg.1).ok_or_else(|| bad_id(seg.1))?[axis];
        let seg_width = seg_max - seg_min - &seg_min_size;
        let rhs = seg_min + &seg_min_size + seg_width / &anchor_seg;
        for node in nodes.iter() {
            let var = &self.variables.get(node).ok_or_else(|| bad_id(*node))?[axis];
            // get the constant width or height
            let size = infos.get(node).unwrap().dimensions[axis] as f32;
            // constraint that x + w * anchor = x_seg_min + w_seg_min + w_seg * anchor_seg
            let offset = Int::from_u64(self.ctx, (anchor * size).round() as u64);
            let z3_cst = &rhs - &offset;
            let z3_cst = var._eq(&z3_cst);
            self.constraints.push(z3_cst);
        }
        Ok(())
    }

    fn set_order(
        &mut self,
        nodes: &[NodeID],
        infos: &HashMap<NodeID, NodesSolverInfo>,
        local_dists: &HashMap<(NodeID, NodeID), usize>,
        axis: usize,
    ) -> Result<(), Graph2SvgErr> {
        let bad_id = |id| {
            let constr = if axis == X_AXIS {
                Constraint::LeftToRight(nodes.to_vec())
            } else {
                Constraint::TopToBottom(nodes.to_vec())
            };
            Graph2SvgErr::BadIDInConstraint(constr.to_string(), id)
        };
        for pair in nodes.windows(2) {
            let n1 = &pair[0];
            let n2 = &pair[1];
            // get z3 variables corresponding to x or y for both nodes
            let var1 = &self.variables.get(n1).ok_or_else(|| bad_id(*n1))?[axis];
            let var2 = &self.variables.get(n2).ok_or_else(|| bad_id(*n2))?[axis];
            let info = infos.get(n1).unwrap();
            // constraint that x2 - x1 > w1 + safety_dist
            let dif = var2 - var1;
            let safety_dist = *local_dists.get(&(*n1, *n2)).unwrap_or(&info.safety_dist);
            let rhs = info.dimensions[axis] as u64 + safety_dist as u64;
            // println!("safety_dist = {safety_dist}");
            let z3_cst = dif.ge(&Int::from_u64(self.ctx, rhs));

            // println!("order = {z3_cst}");
            self.constraints.push(z3_cst);
        }
        Ok(())
    }

    fn set_non_overlap(
        mut self,
        infos: &HashMap<NodeID, NodesSolverInfo>,
        local_dists: &HashMap<(NodeID, NodeID), usize>,
    ) -> Self {
        let vars: Vec<(NodeID, usize, &[NodeID], [&Int<'_>; 2], [Int<'_>; 2])> = infos
            .iter()
            .map(|(id, vb)| {
                let dims = vb.dimensions;
                let var = self.variables.get(id).unwrap();
                let width = Int::from_u64(self.ctx, dims.x as u64);
                let height = Int::from_u64(self.ctx, dims.y as u64);
                (
                    *id,
                    vb.safety_dist,
                    vb.parents.as_slice(),
                    [&var[X_AXIS], &var[Y_AXIS]],
                    [width, height],
                )
            })
            .collect();
        let constrs = vars.iter().enumerate().flat_map(
            |(i, (id1, safety_dist, parents1, [x1, y1], [w1, h1]))| {
                vars.iter()
                    .take(i)
                    // collision is ok for nesting and not ok otherwhise
                    .filter(move |(id2, _, parents2, _, _)| {
                        !(parents2.contains(id1) || parents1.contains(id2))
                    })
                    .map(move |(id2, _, _, [x2, y2], [w2, h2])| {
                        let safety_dist =
                            *local_dists.get(&(*id1, *id2)).unwrap_or(safety_dist) as u64;
                        let w1 = w1 + Int::from_u64(self.ctx, safety_dist);
                        let w2 = w2 + Int::from_u64(self.ctx, safety_dist);
                        let h1 = h1 + Int::from_u64(self.ctx, safety_dist);
                        let h2 = h2 + Int::from_u64(self.ctx, safety_dist);

                        let test_x12 = x1.le(x2) & (*x2 - *x1).lt(&w1);
                        let test_x21 = x2.le(x1) & (*x1 - *x2).lt(&w2);
                        let test_y12 = y1.le(y2) & (*y2 - *y1).lt(&h1);
                        let test_y21 = y2.le(y1) & (*y1 - *y2).lt(&h2);
                        let intersect_x = test_x12 | test_x21;
                        let intersect_y = test_y12 | test_y21;
                        !(intersect_x & intersect_y)
                    })
            },
        );
        self.constraints.extend(constrs);
        self
    }
    fn set_bounds(
        mut self,
        infos: &HashMap<NodeID, NodesSolverInfo>,
        window: Vec2,
        margin: Vec2,
    ) -> Self {
        let margin_x = Int::from_u64(self.ctx, margin.x as u64);
        let margin_y = Int::from_u64(self.ctx, margin.y as u64);
        // tricky corner case when the safety_dist is pixel perfect
        let window_x = Int::from_u64(self.ctx, window.x as u64);
        let window_y = Int::from_u64(self.ctx, window.y as u64);
        let constrs = infos.iter().flat_map(|(id, info)| {
            let var = self.variables.get(id).unwrap();
            let dims = info.dimensions;
            if window.is_smaller(dims + margin) {
                panic!("Margin {margin} is too big !");
            }

            let (min_x, min_y, max_x, max_y) = if let Some(parent_id) = info.parents.last() {
                let parent_var = self.variables.get(parent_id).unwrap();
                let parent = infos.get(parent_id).unwrap();
                let margin = parent.margin;
                let parent_dims = parent.dimensions;
                assert!(
                    parent_dims.is_bigger(margin + dims),
                    "parent dims = {parent_dims}, margin = {margin}, dims = {dims}"
                );
                let max_x = (parent_dims.x - margin.x - dims.x) as u64;
                let max_y = (parent_dims.y - margin.y - dims.y) as u64;
                let max_x = Int::from_u64(self.ctx, max_x);
                let max_y = Int::from_u64(self.ctx, max_y);
                let margin_x = Int::from_u64(self.ctx, margin.x as u64);
                let margin_y = Int::from_u64(self.ctx, margin.y as u64);
                (
                    &parent_var[X_AXIS] + &margin_x,
                    &parent_var[Y_AXIS] + &margin_y,
                    &parent_var[X_AXIS] + &max_x,
                    &parent_var[Y_AXIS] + &max_y,
                )
            } else {
                (
                    margin_x.clone(),
                    margin_y.clone(),
                    &window_x - &Int::from_u64(self.ctx, (margin.x + dims.x) as u64),
                    &window_y - &Int::from_u64(self.ctx, (margin.y + dims.y) as u64),
                )
            };
            // println!("Bounds = {:?}", var[X_AXIS].ge(&min_x));
            // println!("Bounds = {:?}", var[Y_AXIS].ge(&min_y));
            // println!("Bounds = {:?}", var[X_AXIS].le(&max_x));
            // println!("Bounds = {:?}", var[Y_AXIS].le(&max_y));
            [
                var[X_AXIS].ge(&min_x),
                var[Y_AXIS].ge(&min_y),
                var[X_AXIS].le(&max_x),
                var[Y_AXIS].le(&max_y),
            ]
        });
        self.constraints.extend(constrs);

        self
    }

    fn objective_center(
        &self,
        infos: &HashMap<NodeID, NodesSolverInfo>,
        edges: &[Edge],
        window: Vec2,
    ) -> Int {
        // try to center each node in it's parent
        let zero = Int::from_u64(self.ctx, 0);
        let mut dist = Int::from_u64(self.ctx, 0);
        for (id, [x, y]) in self.variables.iter() {
            let info = infos.get(id).unwrap();
            let dims = info.dimensions;
            let w2 = Int::from_u64(self.ctx, (dims.x / 2) as u64);
            let h2 = Int::from_u64(self.ctx, (dims.y / 2) as u64);
            let parents = &info.parents;
            let (cx, cy) = if let Some(parent) = parents.last() {
                let parent_var = self.variables.get(parent).unwrap();
                let parent_dims = infos.get(parent).unwrap().dimensions;
                let parent_w2 = Int::from_u64(self.ctx, (parent_dims.x / 2) as u64);
                let parent_h2 = Int::from_u64(self.ctx, (parent_dims.y / 2) as u64);
                (
                    &parent_var[X_AXIS] + &parent_w2,
                    &parent_var[Y_AXIS] + &parent_h2,
                )
            } else {
                (
                    Int::from_u64(self.ctx, (window.x / 2) as u64),
                    Int::from_u64(self.ctx, (window.y / 2) as u64),
                )
            };
            let dx = x + &w2 - &cx;
            let dx_abs = dx.ge(&zero).ite(&dx, &(-&dx));
            let dy = y + &h2 - &cy;
            let dy_abs = dy.ge(&zero).ite(&dy, &(-&dy));
            dist += dx_abs + dy_abs;
        }
        // group together the nodes that will be connected
        edges
            .iter()
            .filter_map(|e| {
                let n1 = e.from_node();
                let n2 = e.to_node();
                (n1 != n2).then_some((n1, n2))
            })
            .for_each(|(n1, n2)| {
                if let (Some(n1), Some(n2)) = (self.variables.get(&n1), self.variables.get(&n2)) {
                    let (x1, x2) = (&n1[X_AXIS], &n2[X_AXIS]);
                    let (y1, y2) = (&n1[Y_AXIS], &n2[Y_AXIS]);
                    let dx = x1 - x2;
                    let dy = y1 - y2;
                    let dx_abs = dx.ge(&zero).ite(&dx, &(-&dx));
                    let dy_abs = dy.ge(&zero).ite(&dy, &(-&dy));
                    dist += dx_abs + dy_abs;
                }
            });
        dist
    }
}

#[derive(Debug)]
pub struct NodesSolverInfo {
    pub dimensions: Vec2,
    pub margin: Vec2,
    pub parents: Vec<NodeID>,
    pub is_leaf: bool,
    pub safety_dist: usize,
}

impl SolverZ3 {
    pub fn new() -> Self {
        let mut config = Config::new();
        config.set_timeout_msec(TIMEOUT_MS);
        let context = Context::new(&config);
        Self { context }
    }

    pub fn solve_positions(
        self,
        constraints: &[Constraint],
        edges: &[Edge],
        nodes: HashMap<NodeID, NodesSolverInfo>,
        window: Vec2,
        margin: Vec2,
        local_safety: HashMap<(NodeID, NodeID), usize>,
    ) -> Result<HashMap<NodeID, Vec2>, Graph2SvgErr> {
        let params = SolverParams::new(&self.context, &nodes)
            .set_bounds(&nodes, window, margin)
            .set_non_overlap(&nodes, &local_safety)
            .set_constraints(constraints, &nodes, &local_safety)?;

        let optimizer = Optimize::new(&self.context);
        params
            .constraints
            .iter()
            .for_each(|constr| optimizer.assert(constr));

        // println!("{:#?}", nodes);
        // println!("{:#?}", params.constraints);

        println!("Solver running ...");
        optimizer.minimize(&params.objective_center(&nodes, edges, window));
        let status = optimizer.check(&[]);
        type S = SatResult;
        match status {
            S::Sat => {
                println!("Solver found an optimal solution!");
            }
            S::Unknown => {
                println!("Solver has found a solution after timeout of {TIMEOUT_MS} millisecs.",);
            }
            S::Unsat => {
                return Err(Graph2SvgErr::SolverCantFind);
            }
        }
        let model = optimizer.get_model().unwrap();
        let completed = true;

        Ok(params
            .variables
            .into_iter()
            .map(|(node, [x, y])| {
                let x = model.eval(&x, completed).unwrap().as_u64().unwrap() as i32;
                let y = model.eval(&y, completed).unwrap().as_u64().unwrap() as i32;
                (node, Vec2 { x, y })
            })
            .collect())
    }
}
