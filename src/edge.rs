use std::collections::{BTreeMap, HashMap};

use svg::node::element::path::Data;
use svg::node::element::{Group, Path};
use svg::Document;

use crate::helpers::{svg_text, text_dimensions};
use crate::{
    constraints::Anchor,
    error::Graph2SvgErr,
    helpers::{CollisionMap, Matrix, Vec2, Viewbox, NODE_PENALTY},
    node::{Node, NodeID, NodesPlacements, DEFAULT_MARGIN},
    style::Styler,
};

pub use crate::helpers::{Direction, DockSelection};

/// Choose how to connect nodes.
#[derive(Debug, Clone, Copy, Default)]
pub enum EdgeLinkPolicy {
    /// Each node has 2 docking points, 1 for in arrow, 1 for out arrow.
    FactorizeIO,

    /// Each node has 4 x n docking points (n per face / (n+1), n is computed per nodes
    /// depending on how many connections it got, usually 1 per center so 4 docking points)
    #[default]
    MaximizeDockPoints,
}

#[derive(Debug, Clone)]
enum ArrowKind {
    NoMarker = 0,
    EndMarker = 1,
    TwoMarkers = 2,
}

/// Information for styling and pathfinding of how to connect one node to another.
#[derive(Debug, Clone)]
pub struct Edge {
    from: NodeID,
    to: NodeID,
    from_dock: Option<DockSelection>,
    to_dock: Option<DockSelection>,
    label: Option<String>,
    label_anchor: Anchor,
    label_above: bool,
    pub(crate) label_vertical: bool,
    is_arrow: ArrowKind,
}

/// Default [Edge], straight line without a marker at the end.
pub fn edge(from: NodeID, to: NodeID) -> Edge {
    Edge {
        from,
        to,
        from_dock: None,
        to_dock: None,
        label: None,
        label_anchor: Anchor::default(),
        label_above: false,
        label_vertical: false,
        is_arrow: ArrowKind::NoMarker,
    }
}

/// Default [Edge] with a marker at the end to draw an arrow.
pub fn arrow(from: NodeID, to: NodeID) -> Edge {
    let edg = edge(from, to);
    Edge {
        is_arrow: ArrowKind::EndMarker,
        ..edg
    }
}

/// Default [Edge] with 2 markers at the start and end to draw a bidirectionnal arrow.
pub fn arrow_bi(from: NodeID, to: NodeID) -> Edge {
    let edg = edge(from, to);
    Edge {
        is_arrow: ArrowKind::TwoMarkers,
        ..edg
    }
}

impl Edge {
    pub fn new(from: NodeID, to: NodeID) -> Edge {
        edge(from, to)
    }
    /// Change the default start point of the arrow.
    pub fn with_from_dock(mut self, from_dock: DockSelection) -> Self {
        self.from_dock = Some(from_dock);
        self
    }

    /// Change the default end point of the arrow.
    pub fn with_to_dock(mut self, to_dock: DockSelection) -> Self {
        self.to_dock = Some(to_dock);
        self
    }

    /// Add a label to your edge.
    pub fn with_label<T: ToString>(mut self, label: T) -> Self {
        self.label = Some(label.to_string());
        self
    }

    /// Set the label anchor to one segment part of the path.
    pub fn with_label_anchor(mut self, anchor: Anchor, above: bool) -> Self {
        self.label_anchor = anchor;
        self.label_above = above;
        self
    }

    /// Add a label to your edge that will be rotated of 90 degree.
    pub fn with_vertical_label<T: ToString>(mut self, label: T) -> Self {
        self.label = Some(label.to_string());
        self.label_vertical = true;
        self
    }

    pub fn get_label(&self) -> Option<&String> {
        self.label.as_ref()
    }

    fn get_label_pos(
        &self,
        points: &[Vec2],
        styler: &mut Styler,
        label: &str,
    ) -> Result<Vec2, Graph2SvgErr> {
        assert!(points.len() > 1);
        let med = (self.label_anchor.to_float()? * (points.len() as f32)) as usize;
        let med = med.max(1);
        let (p1, p2) = (points[med - 1], points[med]);
        let dir = (p2 - p1).signum();
        let text_center = (p1 + p2) / 2;
        let orthogonal = if self.label_above {
            dir.rotate_right()
        } else {
            dir.rotate_left()
        };
        let margin = text_dimensions(
            label,
            styler.edge_text_style(self.from, self.to)?.clone(),
            &mut styler.fonts_cache,
            self.label_vertical,
        )? / 2
            + DEFAULT_MARGIN;
        Ok(text_center + orthogonal * margin)
    }

    pub fn from_node(&self) -> NodeID {
        self.from
    }

    pub fn to_node(&self) -> NodeID {
        self.to
    }

    pub(crate) fn draw_edge(
        self,
        svg_doc: Document,
        points: Vec<Vec2>,
        styler: &mut Styler,
    ) -> Result<Document, Graph2SvgErr> {
        let mut data = Data::new().move_to((points[0].x, points[0].y));
        for p in points.iter().skip(1) {
            data = data.line_to((p.x, p.y));
        }
        let [style_class, label_class, marker_id, marker_id_rev] =
            styler.edge_classes(self.from, self.to);
        let path = Path::new().set("d", data).set("class", style_class);

        let path = match &self.is_arrow {
            ArrowKind::NoMarker => path,
            ArrowKind::EndMarker => path.set("marker-end", format!("url(#{marker_id})")),
            ArrowKind::TwoMarkers => path
                .set("marker-end", format!("url(#{marker_id})"))
                .set("marker-start", format!("url(#{marker_id_rev})")),
        };

        Ok(match &self.label {
            Some(label) => {
                let position = self.get_label_pos(&points, styler, label)?;
                let label = svg_text(label, &label_class, self.label_vertical, position);
                let group = Group::new().add(path).add(label);
                svg_doc.add(group)
            }
            None => svg_doc.add(path),
        })
    }
}

pub(crate) struct PathFinder {
    policy: EdgeLinkPolicy,
    collision: CollisionMap,
    positions: NodesPlacements,
    dock_points: HashMap<NodeID, [Vec<Vec2>; 2]>,
}

impl PathFinder {
    pub fn new(root: &Node, policy: EdgeLinkPolicy, edges: &[Edge]) -> Result<Self, Graph2SvgErr> {
        let collision = CollisionMap::new(root);
        let mut positions = HashMap::new();
        root.get_viewboxes(&mut positions);
        let mut counts_edge_per_node: HashMap<NodeID, usize> = HashMap::new();
        edges.iter().for_each(|e| {
            *counts_edge_per_node.entry(e.from).or_insert(0) += 1;
            *counts_edge_per_node.entry(e.to).or_insert(0) += 1;
        });

        // build dock point according to the choosen policy
        let mut dock_points: HashMap<NodeID, [Vec<Vec2>; 2]> = HashMap::new();
        let default_docking = DockSelection::default();
        for (node, count) in counts_edge_per_node {
            // default diviser is 2 (i.e center faces)
            let dock_count = DockSelection::all_faces(1 + (count as i32 + 3) / 4);
            let selection = match &policy {
                EdgeLinkPolicy::FactorizeIO => &default_docking,
                EdgeLinkPolicy::MaximizeDockPoints => &dock_count,
            };
            let points = positions
                .get(&node)
                .ok_or_else(|| {
                    let edge = edges
                        .iter()
                        .find(|e| e.from == node || e.to == node)
                        .unwrap();
                    Graph2SvgErr::BadIDInEdge(edge.from, edge.to, node)
                })?
                .dock_points(selection, &collision);
            dock_points.insert(node, [points.clone(), points]);
        }

        Ok(PathFinder {
            policy,
            collision,
            positions,
            dock_points,
        })
    }
    fn remove_dock(&mut self, node: &NodeID, point: &Vec2, in_or_out: usize) {
        let docks = &mut self.dock_points.get_mut(node).unwrap()[in_or_out];
        let opt_idx = docks
            .iter()
            .enumerate()
            .find(|(_, coord)| *coord == point)
            .map(|(idx, _)| idx);
        if let Some(idx) = opt_idx {
            docks.remove(idx);
        }
    }

    pub fn sort_edges(&self, edges: &mut [Edge]) {
        edges.sort_by_key(|e| {
            let vb_a = self.positions.get(&e.from).unwrap();
            let vb_b = self.positions.get(&e.to).unwrap();
            let coord_a = vb_a.topleft + vb_a.dimensions / 2;
            let coord_b = vb_b.topleft + vb_b.dimensions / 2;
            Vec2::manhattan(&coord_a, &coord_b)
        });
    }

    pub fn find(&mut self, edge: &Edge) -> Result<Vec<Vec2>, Graph2SvgErr> {
        const IN_IDX: usize = 0;
        const OUT_IDX: usize = 1;
        println!("Edge from Node {} to Node {}", edge.from, edge.to);
        let from = self.positions.get(&edge.from).unwrap();
        let tmp_starts = if let Some(selection) = &edge.from_dock {
            from.dock_points(selection, &self.collision)
        } else {
            vec![]
        };
        let starts = if edge.from_dock.is_some() {
            &tmp_starts
        } else {
            &self.dock_points.get(&edge.from).unwrap()[OUT_IDX]
        };
        let to = self.positions.get(&edge.to).unwrap();
        let tmp_ends = if let Some(selection) = &edge.to_dock {
            to.dock_points(selection, &self.collision)
        } else {
            vec![]
        };
        let ends = if edge.to_dock.is_some() {
            &tmp_ends
        } else {
            &self.dock_points.get(&edge.to).unwrap()[IN_IDX]
        };
        // println!("starts = {starts:?}");
        // println!("ends = {ends:?}");
        let points = match try_connect(from, to, starts, ends, &self.collision) {
            Some(p) => p,
            None => {
                println!("fallback to dijkstra...");
                dijkstra(from, starts, ends, &self.collision).ok_or(
                    Graph2SvgErr::PathFindingFailed(edge.from, edge.to, MAX_TURN),
                )?
            }
        };
        // println!("points = {points:?}");
        // println!("-----------------------------------------------------");

        // update the collisons and apply docking policy
        self.collision.add_edge_penalty(&points);
        type P = EdgeLinkPolicy;
        let p_start = points.first().unwrap();
        let p_end = points.last().unwrap();
        match &self.policy {
            P::FactorizeIO => {
                let end_len = ends.len();
                // set the only departure point to the founded start
                // and the only arrival point to the founded end
                if starts.len() > 1 {
                    self.dock_points.get_mut(&edge.from).unwrap()[OUT_IDX] = vec![*p_start];
                }
                if end_len > 1 {
                    self.dock_points.get_mut(&edge.to).unwrap()[IN_IDX] = vec![*p_end];
                }
            }
            P::MaximizeDockPoints => {
                // in this policy in and out are shared and only one arrow per dock point
                self.remove_dock(&edge.from, p_start, IN_IDX);
                self.remove_dock(&edge.from, p_start, OUT_IDX);
                self.remove_dock(&edge.to, p_end, IN_IDX);
                self.remove_dock(&edge.to, p_end, OUT_IDX);
            }
        }
        Ok(points)
    }
}

fn compact_path(end: Vec2, prev: Matrix<Vec2>, starts: &[Vec2]) -> Vec<Vec2> {
    let mut path = vec![end];
    let mut next = prev.get_v(&end);
    let mut old_dir = next - end;
    let mut cur = end;
    while !starts.contains(&cur) {
        next = prev.get_v(&cur);
        let dir = next - cur;
        if dir.dot(&old_dir) == 0 {
            path.push(cur);
            old_dir = dir;
        }
        cur = next;
    }
    path.push(cur);
    // since we pushed from end to start we must reverse
    path.reverse();
    path
}
type Priority = (u32, u32, i32);
const MAX_TURN: u32 = 8;
fn dijkstra(
    from: &Viewbox,
    starts: &[Vec2],
    ends: &[Vec2],
    collision: &CollisionMap,
) -> Option<Vec<Vec2>> {
    let zero = Vec2::zero();
    let window = collision.dimensions();
    let mut prev = Matrix::filled(window, Vec2::zero());
    let mut dists = Matrix::filled(window, (u32::MAX, u32::MAX, i32::MAX));
    let mut queue: BTreeMap<Priority, (Vec2, Vec2)> = BTreeMap::new();
    // init queue with starts
    for start in starts.iter() {
        let dist = ends
            .iter()
            .map(|end| Vec2::manhattan(start, end))
            .min()
            .unwrap();
        let cost = (collision.get_v(start), 0, dist);
        dists.set_v(start, cost);
        let dir = from.direction_of(start);
        queue.insert(cost, (*start, dir));
    }
    while let Some(((cost, nturns, _dist), (cur, dir))) = queue.pop_first() {
        if ends.contains(&cur) {
            return Some(compact_path(cur, prev, starts));
        }
        // println!("{cur} dir={dir} dist={_dist} nturns={nturns} cost={cost}");

        [
            (cur + dir, nturns),
            (cur + dir.rotate_left(), nturns + 1),
            (cur + dir.rotate_right(), nturns + 1),
        ]
        .into_iter()
        .filter(|(nei, nei_turns)| {
            nei.is_bigger(zero)
                && nei.is_smaller(window)
                && nei_turns < &MAX_TURN
                && collision.is_ok_start(nei)
        })
        .for_each(|(nei, nei_turns)| {
            let newpriority = (
                cost + collision.get_v(&nei),
                nei_turns,
                ends.iter()
                    .map(|end| Vec2::manhattan(&nei, end))
                    .min()
                    .unwrap(),
            );
            if newpriority <= dists.get_v(&nei) {
                dists.set_v(&nei, newpriority);
                prev.set_v(&nei, cur);
                queue.insert(newpriority, (nei, nei - cur));
            }
        });
    }

    None
}

fn try_connect(
    from: &Viewbox,
    to: &Viewbox,
    starts: &[Vec2],
    ends: &[Vec2],
    collision: &CollisionMap,
) -> Option<Vec<Vec2>> {
    starts
        .iter()
        .flat_map(|start| {
            let start_dir = from.direction_of(start);
            ends.iter().filter_map(move |end| {
                let end_dir = to.direction_of(end);
                if (start.x == end.x || start.y == end.y) && start_dir == -end_dir {
                    path_1_line(start, end, collision)
                } else if start_dir.dot(&end_dir) == 0 {
                    let lines2 = path_2_line(start, end, collision, &start_dir);
                    if lines2.is_some() {
                        lines2
                    } else {
                        path_4_lines(start, end, collision, &start_dir, &end_dir)
                    }
                } else {
                    path_3_line(start, end, collision, &start_dir)
                }
            })
        })
        .min_by_key(|(points, score)| {
            (
                points.len(),
                *score,
                points
                    .windows(2)
                    .map(|pair| Vec2::manhattan(&pair[0], &pair[1]))
                    .sum::<i32>(),
            )
        })
        .map(|t| t.0)
}

fn path_1_line(start: &Vec2, end: &Vec2, collision: &CollisionMap) -> Option<(Vec<Vec2>, u32)> {
    if start == end {
        return None;
    }
    let score = if start.x == end.x {
        let x = start.x;
        let mut score = 0;
        for y in start.y.min(end.y)..start.y.max(end.y) {
            let cost = collision.get(x as usize, y as usize);
            if cost >= NODE_PENALTY {
                return None;
            }
            score += cost;
        }
        score
    } else {
        assert!(start.y == end.y);
        let y = start.y;
        let mut score = 0;
        for x in start.x.min(end.x)..start.x.max(end.x) {
            let cost = collision.get(x as usize, y as usize);
            if cost >= NODE_PENALTY {
                return None;
            }
            score += cost;
        }
        score
    };
    Some((vec![*start, *end], score))
}

fn path_2_line(
    start: &Vec2,
    end: &Vec2,
    collision: &CollisionMap,
    start_dir: &Vec2,
) -> Option<(Vec<Vec2>, u32)> {
    if start == end {
        return None;
    }
    let mid = ((end - start) * start_dir.abs()) + start;
    // safe check for collision in the 2 straights lines
    let (_, s1) = path_1_line(start, &mid, collision)?;
    let (_, s2) = path_1_line(&mid, end, collision)?;
    Some((vec![*start, mid, *end], s1 + s2))
}

const N_MAX_OVERLAP: i32 = 10;
const ZERO: Vec2 = Vec2 { x: 0, y: 0 };

fn find_margin(start: &Vec2, start_dir: &Vec2, collision: &CollisionMap) -> Option<Vec2> {
    let dims = collision.dimensions();
    (1..N_MAX_OVERLAP).find_map(|i| {
        let dist = start_dir * DEFAULT_MARGIN * i;
        let coord = start + dist;
        (coord.is_bigger(ZERO) && coord.is_smaller(dims) && collision.get_v(&coord) == 0)
            .then_some(dist)
    })
}

fn path_3_line(
    start: &Vec2,
    end: &Vec2,
    collision: &CollisionMap,
    start_dir: &Vec2,
) -> Option<(Vec<Vec2>, u32)> {
    if start == end {
        return None;
    }
    let dif = end - start;
    let projection = start_dir.abs();
    let dist: Vec2 = (dif * projection) / 2;
    let dist = if dist.abs().is_smaller(DEFAULT_MARGIN) {
        find_margin(start, start_dir, collision)?
    } else {
        dist
    };
    let mid1 = dist + start;
    let mid2 = mid1 + dif * (-projection + 1);
    let (_, s1) = path_1_line(start, &mid1, collision)?;
    let (_, s2) = path_1_line(&mid1, &mid2, collision)?;
    let (_, s3) = path_1_line(&mid2, end, collision)?;
    Some((vec![*start, mid1, mid2, *end], s1 + s2 + s3))
}

fn path_4_lines(
    start: &Vec2,
    end: &Vec2,
    collision: &CollisionMap,
    start_dir: &Vec2,
    end_dir: &Vec2,
) -> Option<(Vec<Vec2>, u32)> {
    if start == end {
        return None;
    }
    let p1 = start + find_margin(start, start_dir, collision)?;
    let p2 = end + find_margin(end, end_dir, collision)?;
    let middle1 = Vec2 { x: p1.x, y: p2.y };
    let middle2 = Vec2 { x: p2.x, y: p1.y };
    let (_, l1) = path_1_line(start, &p1, collision)?;
    let (_, l4) = path_1_line(&p2, end, collision)?;

    // test both paths if one of them is ok return it
    let path_m1 = path_1_line(&p1, &middle1, collision);
    let path_m1_n = path_1_line(&middle1, &p2, collision);
    let (mid, l23) = if let (Some((_, s2)), Some((_, s3))) = (path_m1, path_m1_n) {
        (middle1, s2 + s3)
    } else {
        let (_, s2) = path_1_line(&p1, &middle2, collision)?;
        let (_, s3) = path_1_line(&middle2, &p2, collision)?;
        (middle2, s2 + s3)
    };

    Some((vec![*start, p1, mid, p2, *end], l1 + l23 + l4))
}

#[cfg(test)]
mod test_edge {
    use std::collections::HashMap;

    use crate::{
        helpers::{DOWN, LEFT, RIGHT},
        node::{array2d, Node, NodeContent},
        shape::Shape,
        DEFAULT_SAFETY_DIST,
    };

    use super::*;

    #[test]
    fn test_cfg_bb1_to_bb1() {
        let array = array2d(0, vec![vec!["bonsoir"], vec!["hi"], vec!["coucou"]]);
        let mut root = Node::new(
            100,
            NodeContent::Container(vec![array], DEFAULT_SAFETY_DIST),
            Shape::None,
        );
        root.infer_dimensions(&mut Styler::new(), &[], &[]).unwrap();
        let edges = [edge(1, 2), edge(2, 3)];
        root.root_solve_positions(vec![], &edges, HashMap::new(), vec![])
            .unwrap();
        let mut pathfinder = PathFinder::new(&root, EdgeLinkPolicy::default(), &edges).unwrap();
        let points12 = pathfinder.find(&edges[0]).unwrap();
        check_bb1_to_bb1(points12, pathfinder.positions.get(&1).unwrap());
        println!("edge(1,2) is ok!");
        let points23 = pathfinder.find(&edges[1]).unwrap();
        check_bb1_to_bb1(points23, pathfinder.positions.get(&2).unwrap());
    }

    fn check_bb1_to_bb1(points: Vec<Vec2>, cell: &Viewbox) {
        // This test expect the following path (or the symmetric one with the left side)
        // +----------+
        // |          |
        // |          | ------+
        // |          |       |
        // +----------+       |
        // |          |       |
        // |          | <-----+
        // |          |
        // +----------+
        let left = cell.topleft + LEFT + DOWN * (cell.dimensions / 2);
        let expected1 = vec![
            left,
            left + LEFT * DEFAULT_MARGIN,
            left + LEFT * DEFAULT_MARGIN + DOWN * cell.dimensions,
            left + DOWN * cell.dimensions,
        ];
        let right = left + RIGHT * (cell.dimensions + 2);
        let expected2 = vec![
            right,
            right + RIGHT * DEFAULT_MARGIN,
            right + RIGHT * DEFAULT_MARGIN + DOWN * cell.dimensions,
            right + DOWN * cell.dimensions,
        ];
        println!("Given result {points:#?}");
        println!("Expected result1 {expected1:#?}");
        println!("Expected result2 {expected2:#?}");
        assert!(points == expected1 || points == expected2);
    }
}
