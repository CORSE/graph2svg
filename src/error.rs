use std::{
    fmt::{self},
    io,
};

use crate::{
    constraints::DimsConstraint,
    node::NodeID,
    prelude::{Direction, DockSelection},
    Constraint,
};
use font_kit::error::{FontLoadingError, GlyphLoadingError};
use thiserror::Error;

/// Gather (almost) all errors that could occurs when using the library.
#[derive(Error, Debug)]
pub enum Graph2SvgErr {
    #[error("Font {0:?} not found on system.")]
    SearchFont(String),

    #[error("loading font failed {0}")]
    LoadFont(#[from] FontLoadingError),

    #[error("glyph loading failed {0}")]
    Glyph(#[from] GlyphLoadingError),

    #[error("glyph {0} not found in font")]
    SearchGlyph(char),

    #[error("unknown CSS class {0} maybe you forget to add the corresponding style to the Graph?")]
    UnknownCssClass(String),

    #[error("CSS class {0} should be a Style::Text")]
    BadTextStyle(String),

    #[error("IO error {0}")]
    IO(#[from] io::Error),

    #[error("fmt write error {0}")]
    Fmt(#[from] fmt::Error),

    #[error("Node {0} is empty or contains empty lines.")]
    EmptyNode(NodeID),

    #[error("Solver cant find a valid solution. Maybe, some constraints are incompatible.")]
    SolverCantFind,

    #[error("Constraint {0} contains an unknown NodeID {1}")]
    BadIDInConstraint(String, NodeID),

    #[error("Constraint {0:?} contains an unknown NodeID {1}")]
    BadIDInDimsConstraint(DimsConstraint, NodeID),

    #[error("Edge from {0} to {1} contains an unknown NodeID {2}")]
    BadIDInEdge(NodeID, NodeID, NodeID),

    #[error("Constraint {0} is included in Constraint {1}, consider removing one of them.")]
    EquivalentConstraints(String, String),

    #[error("Constraint {0:?} try to align with respect to a segment of non aligned nodes.")]
    InvalidAlignWithSegment(Constraint),

    #[error("Custom anchor with value {0} is invalid. Value should be between 0.0 and 1.0.")]
    InvalidAnchor(f32),

    #[error(
        "Edge PathFinding from {0} to {1} cannot find a path with stricty less than {2} turns."
    )]
    PathFindingFailed(NodeID, NodeID, u32),
}

#[derive(Error, Debug, Clone)]
#[error("Invalid DockSelection {0:?} because of {1}")]
pub struct InvalidDockSelection(pub DockSelection, pub DockError);

#[derive(Error, Debug, Clone)]
pub enum DockError {
    #[error("diviser {0} is too small (should be >= 2).")]
    DiviserTooSmall(i32),
    #[error("faces selection (=directions) is empty.")]
    EmptyDirections,
    #[error("faces selection (=directions) contains duplicates {0:?}.")]
    DuplicateDirections(Direction),
    #[error("segments selection is empty.")]
    EmptySelections,
    #[error("segments selection contains duplicates {0}.")]
    DuplicateSelections(i32),
    #[error("segments selection contains out of band index {0} (should be in [1,diviser[).")]
    OutOfBandSelections(i32),
}
