mod collision_map;
mod vec2;
mod viewbox;

use crate::error::Graph2SvgErr;
use crate::style::TextStyle;
use font_kit::canvas::RasterizationOptions;
use font_kit::family_name::FamilyName;
use font_kit::font::Font;
use font_kit::hinting::HintingOptions;
use font_kit::properties::Properties;
use font_kit::source::SystemSource;
use pathfinder_geometry::transform2d::Transform2F;
use std::collections::HashMap;
use svg::node::Text;

pub use collision_map::*;
pub use vec2::{Vec2, DOWN, LEFT, RIGHT, UP};
pub use viewbox::*;

pub fn text_dimensions(
    text: &str,
    style: TextStyle,
    cache: &mut HashMap<String, Font>,
    is_vertical: bool,
) -> Result<Vec2, Graph2SvgErr> {
    if !cache.contains_key(style.font.as_str()) {
        let font = SystemSource::new()
            .select_best_match(
                &[FamilyName::Title(style.font.clone())],
                &Properties::default(),
            )
            .map_err(|_| Graph2SvgErr::SearchFont(style.font.clone()))?
            .load()?;
        // println!("Font Best match is {font:?}");
        cache.insert(style.font.clone(), font);
    }
    let font = cache.get(style.font.as_str()).unwrap();
    let fontsize = style.font_size as f32;
    let metric = font.metrics();
    let pem = metric.units_per_em as f32;
    let identity = Transform2F::default();
    let mut width: f32 = 0.0;
    let mut height: i32 = 0;
    for c in text.chars() {
        let glyph_id = font.glyph_for_char(c).ok_or(Graph2SvgErr::SearchGlyph(c))?;
        let advance_x = (font.advance(glyph_id)?.x() * fontsize) / pem;
        // raster_bounds under estimate the real rendered char on the x axis
        let rect = font.raster_bounds(
            glyph_id,
            fontsize,
            identity,
            HintingOptions::None,
            RasterizationOptions::Bilevel,
        )?;
        width += advance_x;
        // println!("{c:?} w={}, h={}", advance_x, rect.height());
        height = height.max(rect.height());
    }
    let dims = Vec2 {
        x: width.ceil() as i32,
        y: height,
    };

    Ok(if is_vertical {
        Vec2 {
            x: dims.y,
            y: dims.x,
        }
    } else {
        dims
    })
}

pub(crate) fn svg_text(
    text: &str,
    css_class: &str,
    is_vertical: bool,
    position: Vec2,
) -> svg::node::element::Text {
    let text = svg::node::element::Text::new()
        .set("class", css_class)
        .add(Text::new(text));
    let Vec2 { x, y } = position;
    if is_vertical {
        text.set("transform", format!("translate({x},{y}) rotate(90)"))
    } else {
        text.set("x", x).set("y", y)
    }
}
