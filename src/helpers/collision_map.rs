use crate::helpers::Vec2;
use crate::node::{Node, NodeContent, DEFAULT_MARGIN};

use std::fmt::Debug;

#[derive(Clone)]
pub struct Matrix<T: Copy + Debug> {
    pub width: usize,
    pub height: usize,
    pub buf: Vec<T>,
}

// for testing purpose
impl Default for CollisionMap {
    fn default() -> Self {
        Self(
            Matrix {
                width: 100,
                height: 100,
                buf: vec![0; 100 * 100],
            },
            // Vec2::zero(),
        )
    }
}

pub const NODE_PENALTY: u32 = 1000;
const MARGIN_EDGE: i32 = 5;
const EDGE_PENALTY: u32 = 1;
const MARGIN_PENALTY: u32 = 10;

// matrix + a cache for knowing the smallest node
pub struct CollisionMap(Matrix<u32>);
impl Debug for CollisionMap {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "+{}+", "-".repeat(self.0.width))?;
        for i in 0..self.0.height {
            write!(f, "|")?;
            for j in 0..self.0.width {
                let cost = self.0.get(j, i);
                let c = if cost < EDGE_PENALTY {
                    ' '
                } else if cost < MARGIN_PENALTY {
                    'o'
                } else if cost < NODE_PENALTY {
                    '.'
                } else {
                    '#'
                };
                write!(f, "{c}")?;
            }
            writeln!(f, "|")?;
        }
        writeln!(f, "+{}+", "-".repeat(self.0.width))?;
        writeln!(f, "Width={}, Height={}", self.0.width, self.0.height)
    }
}
impl CollisionMap {
    // #[inline]
    // pub fn smallest_rect(&self) -> Vec2 {
    //     self.1
    // }

    #[inline]
    pub fn dimensions(&self) -> Vec2 {
        self.0.dimensions()
    }
    #[inline]
    pub fn is_ok_start(&self, p: &Vec2) -> bool {
        self.get_v(p) < NODE_PENALTY
    }
    #[inline]
    fn set_i(&mut self, x: i32, y: i32, cost: u32) {
        self.0.buf[y as usize * self.0.width + x as usize] = cost;
    }

    #[inline]
    fn increment_v(&mut self, coord: Vec2, cost: u32) {
        self.0.buf[coord.y as usize * self.0.width + coord.x as usize] += cost;
    }

    #[inline]
    pub fn get(&self, x: usize, y: usize) -> u32 {
        self.0.get(x, y)
    }
    #[inline]
    pub fn get_v(&self, coord: &Vec2) -> u32 {
        self.0.get_v(coord)
    }

    pub fn new(root: &Node) -> Self {
        let dimensions = root.viewbox().dimensions;
        let width = dimensions.x as usize;
        let height = dimensions.y as usize;
        let mut map = Self(
            Matrix {
                width,
                height,
                buf: vec![0; (width + 1) * (height + 1)],
            },
            // dimensions,
        );
        map.add_node_penaly(root);
        map
    }

    /// Recursively add leaf nodes to the CollisionMap
    fn add_node_penaly(&mut self, node: &Node) {
        let viewbox = node.viewbox();
        // if viewbox.dimensions.is_smaller(self.1) {
        //     self.1 = viewbox.dimensions;
        // }
        type C = NodeContent;
        match node.content() {
            C::Text(..) => {
                let dims = viewbox.dimensions;
                let topleft = viewbox.topleft;
                let min_y = (topleft.y - DEFAULT_MARGIN.y).max(0);
                let max_y = (topleft.y + DEFAULT_MARGIN.y + dims.y).min(self.0.height as i32);
                let min_x = (topleft.x - DEFAULT_MARGIN.x).max(0);
                let max_x = (topleft.x + DEFAULT_MARGIN.x + dims.x).min(self.0.width as i32);
                for y in min_y..=max_y {
                    for x in min_x..=max_x {
                        if self.0.get(x as usize, y as usize) == 0 {
                            self.set_i(x, y, MARGIN_PENALTY);
                        }
                    }
                }
                for y in (topleft.y)..=(topleft.y + dims.y) {
                    for x in (topleft.x)..=(topleft.x + dims.x) {
                        self.set_i(x, y, NODE_PENALTY);
                    }
                }
            }
            C::Array2D(cells, ..) => {
                for line in cells.iter() {
                    for cell in line.iter() {
                        self.add_node_penaly(cell);
                    }
                }
            }
            C::Container(children, ..) => {
                for child in children.iter() {
                    self.add_node_penaly(child);
                }
            }
        }
    }

    pub fn add_edge_penalty(&mut self, path: &[Vec2]) {
        let window = self.dimensions();
        let zero = Vec2::zero();
        for pair in path.windows(2) {
            let prev = pair[0];
            let next = pair[1];
            let step = (next - prev).signum();
            let step_orth = step.rotate_left();
            let mut cur = prev;
            while cur != next {
                for i in (-MARGIN_EDGE)..=MARGIN_EDGE {
                    let coord = cur + step_orth * i;
                    if coord.is_bigger(zero) && coord.is_smaller(window) {
                        self.increment_v(cur + (step_orth * i), EDGE_PENALTY);
                    }
                }
                cur += step;
            }
        }
        let last = path[path.len() - 1];
        let before_last = path[path.len() - 2];
        let step_orth = (last - before_last).rotate_left();
        for i in (-MARGIN_EDGE)..=MARGIN_EDGE {
            let coord = last + step_orth * i;
            if coord.is_bigger(zero) && coord.is_smaller(window) {
                self.increment_v(last + (step_orth * i), EDGE_PENALTY);
            }
        }
    }
}

impl<T: Copy + Debug> Matrix<T> {
    pub fn dimensions(&self) -> Vec2 {
        Vec2 {
            x: self.width as i32,
            y: self.height as i32,
        }
    }

    pub fn filled(dimensions: Vec2, data: T) -> Self {
        let width = dimensions.x as usize;
        let height = dimensions.y as usize;
        Self {
            width,
            height,
            buf: vec![data; (width + 1) * (height + 1)],
        }
    }

    #[inline]
    pub fn get(&self, x: usize, y: usize) -> T {
        self.buf[y * self.width + x]
    }
    #[inline]
    pub fn get_v(&self, v: &Vec2) -> T {
        self.buf[v.y as usize * self.width + v.x as usize]
    }
    #[inline]
    pub fn set_v(&mut self, v: &Vec2, data: T) {
        self.buf[v.y as usize * self.width + v.x as usize] = data;
    }
}

impl<T: Copy + Debug> Debug for Matrix<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for i in 0..self.height {
            for j in 0..self.width {
                write!(f, "{:?}, ", self.get(j, i))?;
            }
            writeln!(f)?;
        }
        writeln!(f, "Width={}, Height={}", self.width, self.height)
    }
}
