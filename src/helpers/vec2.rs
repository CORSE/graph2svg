use std::{
    fmt::Display,
    iter::Sum,
    ops::{Add, AddAssign, Div, Index, Mul, Neg, Sub, SubAssign},
};

pub const LEFT: Vec2 = Vec2 { x: -1, y: 0 };
pub const RIGHT: Vec2 = Vec2 { x: 1, y: 0 };
pub const UP: Vec2 = Vec2 { x: 0, y: -1 };
pub const DOWN: Vec2 = Vec2 { x: 0, y: 1 };

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Vec2 {
    pub x: i32,
    pub y: i32,
}

impl Sum for Vec2 {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        let mut res = Vec2::zero();
        for v in iter {
            res += v;
        }
        res
    }
}

impl<'a> Sum<&'a Vec2> for Vec2 {
    fn sum<I: Iterator<Item = &'a Vec2>>(iter: I) -> Self {
        let mut res = Vec2::zero();
        for v in iter {
            res += v;
        }
        res
    }
}

impl Display for Vec2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(x={}, y={})", self.x, self.y)
    }
}

impl From<(u32, u32)> for Vec2 {
    fn from(value: (u32, u32)) -> Self {
        Vec2 {
            x: value.0 as i32,
            y: value.1 as i32,
        }
    }
}

impl From<(usize, usize)> for Vec2 {
    fn from(value: (usize, usize)) -> Self {
        Vec2 {
            x: value.0 as i32,
            y: value.1 as i32,
        }
    }
}

impl From<Vec2> for (usize, usize) {
    fn from(val: Vec2) -> Self {
        (val.x as usize, val.y as usize)
    }
}

impl From<&(u32, u32)> for Vec2 {
    fn from(value: &(u32, u32)) -> Self {
        Vec2 {
            x: value.0 as i32,
            y: value.1 as i32,
        }
    }
}

impl From<&mut (u32, u32)> for Vec2 {
    fn from(value: &mut (u32, u32)) -> Self {
        Vec2 {
            x: value.0 as i32,
            y: value.1 as i32,
        }
    }
}

impl Index<usize> for Vec2 {
    type Output = i32;
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("Index {index} is out of range for accessing Vec2U"),
        }
    }
}

impl Div for Vec2 {
    type Output = Vec2;
    fn div(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x / rhs.x,
            y: self.y / rhs.y,
        }
    }
}

impl AddAssign<i32> for Vec2 {
    fn add_assign(&mut self, rhs: i32) {
        self.x += rhs;
        self.y += rhs;
    }
}

impl AddAssign<usize> for Vec2 {
    fn add_assign(&mut self, rhs: usize) {
        self.x += rhs as i32;
        self.y += rhs as i32;
    }
}

impl AddAssign for Vec2 {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl AddAssign<&Vec2> for Vec2 {
    fn add_assign(&mut self, rhs: &Vec2) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl SubAssign<usize> for Vec2 {
    fn sub_assign(&mut self, rhs: usize) {
        self.x -= rhs as i32;
        self.y -= rhs as i32;
    }
}

impl From<i32> for Vec2 {
    fn from(value: i32) -> Self {
        Self { x: value, y: value }
    }
}

impl From<usize> for Vec2 {
    fn from(value: usize) -> Self {
        let int = value as i32;
        Self { x: int, y: int }
    }
}

impl Sub for Vec2 {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Sub<&Vec2> for &Vec2 {
    type Output = Vec2;
    fn sub(self, rhs: &Vec2) -> Self::Output {
        Vec2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Sub<i32> for Vec2 {
    type Output = Self;
    fn sub(self, rhs: i32) -> Self::Output {
        Self {
            x: self.x - rhs,
            y: self.y - rhs,
        }
    }
}
impl Add for &Vec2 {
    type Output = Vec2;
    fn add(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Neg for Vec2 {
    type Output = Vec2;
    fn neg(self) -> Self::Output {
        Vec2 {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl Add<Vec2> for &Vec2 {
    type Output = Vec2;
    fn add(self, rhs: Vec2) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Add<&Vec2> for Vec2 {
    type Output = Vec2;
    fn add(self, rhs: &Vec2) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Add<usize> for &Vec2 {
    type Output = Vec2;
    fn add(self, rhs: usize) -> Self::Output {
        let rhs = rhs as i32;
        Vec2 {
            x: self.x + rhs,
            y: self.y + rhs,
        }
    }
}

impl Add<i32> for &Vec2 {
    type Output = Vec2;
    fn add(self, rhs: i32) -> Self::Output {
        Vec2 {
            x: self.x + rhs,
            y: self.y + rhs,
        }
    }
}

impl Add for Vec2 {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Add<usize> for Vec2 {
    type Output = Self;
    fn add(self, rhs: usize) -> Self::Output {
        let rhs = rhs as i32;
        Self {
            x: self.x + rhs,
            y: self.y + rhs,
        }
    }
}

impl Add<i32> for Vec2 {
    type Output = Self;
    fn add(self, rhs: i32) -> Self::Output {
        Self {
            x: self.x + rhs,
            y: self.y + rhs,
        }
    }
}
impl Mul<&Vec2> for &Vec2 {
    type Output = Vec2;
    fn mul(self, rhs: &Vec2) -> Self::Output {
        Vec2 {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}

impl Mul<&Vec2> for Vec2 {
    type Output = Vec2;
    fn mul(self, rhs: &Vec2) -> Self::Output {
        Vec2 {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}

impl Mul<Vec2> for &Vec2 {
    type Output = Vec2;
    fn mul(self, rhs: Vec2) -> Self::Output {
        Vec2 {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}

impl Mul<usize> for &Vec2 {
    type Output = Vec2;
    fn mul(self, rhs: usize) -> Self::Output {
        Vec2 {
            x: self.x * rhs as i32,
            y: self.y * rhs as i32,
        }
    }
}
impl Mul<i32> for &Vec2 {
    type Output = Vec2;
    fn mul(self, rhs: i32) -> Self::Output {
        Vec2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}
impl Mul for Vec2 {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}

impl Mul<usize> for Vec2 {
    type Output = Self;
    fn mul(self, rhs: usize) -> Self::Output {
        Self {
            x: self.x * rhs as i32,
            y: self.y * rhs as i32,
        }
    }
}
impl Mul<i32> for Vec2 {
    type Output = Self;
    fn mul(self, rhs: i32) -> Self::Output {
        Self {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}
impl Div<usize> for Vec2 {
    type Output = Self;
    fn div(self, rhs: usize) -> Self::Output {
        Self {
            x: self.x / rhs as i32,
            y: self.y / rhs as i32,
        }
    }
}

impl Div<i32> for Vec2 {
    type Output = Self;
    fn div(self, rhs: i32) -> Self::Output {
        Self {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl Vec2 {
    pub fn signum(&self) -> Vec2 {
        Self {
            x: self.x.signum(),
            y: self.y.signum(),
        }
    }
    pub fn is_smaller(&self, rhs: Self) -> bool {
        self.x < rhs.x && self.y < rhs.y
    }
    pub fn is_bigger(&self, rhs: Self) -> bool {
        self.x > rhs.x && self.y > rhs.y
    }

    pub fn maximised(self) -> Self {
        let max_xy = self.x.max(self.y);
        Self {
            x: max_xy,
            y: max_xy,
        }
    }

    pub fn zero() -> Self {
        Self { x: 0, y: 0 }
    }

    pub fn dot(&self, rhs: &Self) -> i32 {
        self.x * rhs.x + self.y * rhs.y
    }
    pub fn rotate_right(&self) -> Self {
        Self {
            x: -self.y,
            y: self.x,
        }
    }

    pub fn rotate_left(&self) -> Self {
        Self {
            x: self.y,
            y: -self.x,
        }
    }

    pub fn manhattan(a: &Self, b: &Self) -> i32 {
        (a.x - b.x).abs() + (a.y - b.y).abs()
    }

    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    pub fn abs(&self) -> Self {
        Self {
            x: self.x.abs(),
            y: self.y.abs(),
        }
    }
}

#[cfg(test)]
mod test_vec2 {
    use super::*;
    #[test]
    fn test_maths() {
        let one = Vec2::from(1);
        assert_eq!(LEFT + LEFT, LEFT * 2);
        assert_eq!(LEFT + one, DOWN);
        assert_eq!(LEFT + RIGHT, Vec2::zero());
        assert_eq!(LEFT - LEFT, Vec2::zero());
        assert_eq!(LEFT / 2, Vec2::zero());
        assert_eq!(LEFT * 4 / 2, LEFT * 2);

        assert_eq!(Vec2::manhattan(&LEFT, &DOWN), 2);

        assert_eq!(LEFT.dot(&LEFT), 1);
        assert_eq!(LEFT.dot(&RIGHT), -1);
        assert_eq!(LEFT.dot(&DOWN), 0);

        assert_eq!(LEFT.rotate_left(), DOWN);
        assert_eq!(DOWN.rotate_left(), RIGHT);
        assert_eq!(RIGHT.rotate_left(), UP);
        assert_eq!(UP.rotate_left(), LEFT);

        assert_eq!(LEFT.rotate_right(), UP);
        assert_eq!(UP.rotate_right(), RIGHT);
        assert_eq!(RIGHT.rotate_right(), DOWN);
        assert_eq!(DOWN.rotate_right(), LEFT);
    }
}
