use crate::error::{DockError, InvalidDockSelection};
use crate::helpers::{CollisionMap, Vec2, DOWN, LEFT, RIGHT, UP};
use crate::node::DEFAULT_MARGIN;

#[cfg(doc)]
use crate::edge::Edge;

#[derive(Debug, Clone, Copy)]
pub struct Viewbox {
    pub topleft: Vec2,
    pub dimensions: Vec2,
}

impl Default for Viewbox {
    fn default() -> Self {
        Self {
            topleft: Vec2::zero(),
            dimensions: DEFAULT_MARGIN,
        }
    }
}

/// Simple enum to select faces from Rectangle, useful for overiding [DockSelection] when you want
/// to change the default [Edge] pathfinding.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    Left = 0,
    Top = 1,
    Right = 2,
    Bottom = 3,
}

/// Simple type to select some points on faces of a Rectangle, useful when you want to change the
/// default [Edge] pathfinding.
#[derive(Debug, Clone, PartialEq)]
pub struct DockSelection {
    directions: Vec<Direction>,
    diviser: i32,
    selected: Vec<i32>,
}

const ALL_DIRECTIONS: [Direction; 4] = [
    Direction::Left,
    Direction::Top,
    Direction::Right,
    Direction::Bottom,
];

/// Dummy O(n^2) way to get a duplicate in a Vec
fn get_duplicate<T: Copy + PartialEq>(v: &[T]) -> Option<T> {
    v.iter()
        .enumerate()
        .find(|&(i, d)| v.iter().take(i).any(|d2| d2 == d))
        .map(|(_, &d)| d)
}

impl DockSelection {
    /// Create a new [DockSelection] by selecting:
    /// - By how much divide faces (usually 2)
    /// - Which faces to keep (usually all 4 directions)
    /// - Which points to keep per face (usually all points: `1..diviser`)
    pub fn try_new(
        diviser: i32,
        directions: Vec<Direction>,
        selected: Vec<i32>,
    ) -> Result<Self, InvalidDockSelection> {
        let ds = DockSelection {
            directions,
            diviser,
            selected,
        };
        type DE = DockError;
        if diviser < 2 {
            return Err(InvalidDockSelection(ds, DE::DiviserTooSmall(diviser)));
        }
        if ds.directions.is_empty() {
            return Err(InvalidDockSelection(ds, DE::EmptyDirections));
        }
        if let Some(dir) = get_duplicate(&ds.directions) {
            return Err(InvalidDockSelection(ds, DE::DuplicateDirections(dir)));
        }
        if ds.selected.is_empty() {
            return Err(InvalidDockSelection(ds, DE::EmptySelections));
        }
        if let Some(idx) = get_duplicate(&ds.selected) {
            return Err(InvalidDockSelection(ds, DE::DuplicateSelections(idx)));
        }
        if let Some(&idx) = ds.selected.iter().find(|idx| !(1..diviser).contains(idx)) {
            return Err(InvalidDockSelection(ds, DE::OutOfBandSelections(idx)));
        }
        Ok(ds)
    }

    /// Select all points from all faces cut in `n=diviser` parts.
    pub fn try_all_faces(diviser: i32) -> Result<Self, InvalidDockSelection> {
        Self::try_new(diviser, ALL_DIRECTIONS.to_vec(), (1..diviser).collect())
    }

    /// Select all points from all faces cut in `n=diviser` parts. Can panic like [Self::try_new].
    /// # Examples
    /// Diviser of 2 means to cut in 2 equal part and then select 2 - 1 = 1 point so the middle
    /// point.
    /// ```rust
    /// # use graph2svg::edge::DockSelection;
    /// assert_eq!(DockSelection::all_faces(2), DockSelection::all_centers());
    /// ```
    pub fn all_faces(diviser: i32) -> Self {
        match Self::try_new(diviser, ALL_DIRECTIONS.to_vec(), (1..diviser).collect()) {
            Ok(res) => res,
            Err(err) => panic!("Error: {err}"),
        }
    }

    /// Same as [DockSelection::try_all_faces] but select the directions you want.
    pub fn try_faces(
        diviser: i32,
        directions: Vec<Direction>,
    ) -> Result<Self, InvalidDockSelection> {
        Self::try_new(diviser, directions, (1..diviser).collect())
    }

    /// Same as [DockSelection::all_faces] but select the directions you want. Can panic like
    /// [Self::faces_and_segments].
    pub fn faces(diviser: i32, directions: Vec<Direction>) -> Self {
        match Self::try_new(diviser, directions, (1..diviser).collect()) {
            Ok(res) => res,
            Err(err) => panic!("Error: {err}"),
        }
    }

    /// Select diviser, directions and what point(s) you keep.
    /// # Assertions
    /// This function could panic if one of this assertions is not hold:
    /// - diviser > 1
    /// - directions is not empty
    /// - directions length is at most 4
    /// - selected length is in [1, diviser[
    /// - selected should contains index from 1 to diviser (excluded)
    pub fn faces_and_segments(
        diviser: i32,
        directions: Vec<Direction>,
        selected: Vec<i32>,
    ) -> Self {
        match Self::try_new(diviser, directions, selected) {
            Ok(res) => res,
            Err(err) => panic!("Error: {err}"),
        }
    }

    /// Select some points in all faces.
    pub fn try_segments(diviser: i32, selected: Vec<i32>) -> Result<Self, InvalidDockSelection> {
        Self::try_new(diviser, ALL_DIRECTIONS.to_vec(), selected)
    }

    /// Select some points in all faces. Can panic like [Self::faces_and_segments].
    pub fn segments(diviser: i32, selected: Vec<i32>) -> Self {
        match Self::try_new(diviser, ALL_DIRECTIONS.to_vec(), selected) {
            Ok(res) => res,
            Err(err) => panic!("Error: {err}"),
        }
    }

    /// Center of all faces.
    pub fn all_centers() -> Self {
        Self::default()
    }

    /// Centers of some faces.
    pub fn try_centers(directions: Vec<Direction>) -> Result<Self, InvalidDockSelection> {
        Self::try_faces(2, directions)
    }
    /// Centers of some faces. Can panic like [Self::faces_and_segments].
    pub fn centers(directions: Vec<Direction>) -> Self {
        match Self::try_faces(2, directions) {
            Ok(res) => res,
            Err(err) => panic!("Error: {err}"),
        }
    }
}

impl Default for DockSelection {
    /// Get the center of all faces of the rectangle
    fn default() -> Self {
        DockSelection {
            directions: ALL_DIRECTIONS.to_vec(),
            diviser: 2,
            selected: vec![1],
        }
    }
}

impl Viewbox {
    /// This returns LEFT, UP, RIGHT or DOWN.
    /// It assumes point facing a side of the rectange.
    /// Like this:
    ///    +--+
    ///    |  |
    ///    |  |   * (valid point => facing RIGHT side)
    ///    +--+
    ///           * (invalid point, will return something weird)
    pub fn direction_of(&self, point: &Vec2) -> Vec2 {
        if point.x < self.topleft.x {
            return LEFT;
        }
        if point.y < self.topleft.y {
            return UP;
        }
        if point.x > self.topleft.x + self.dimensions.x {
            return RIGHT;
        }
        DOWN
    }

    /// Points equaly spreaded through all 4 faces with respect to collisons.
    /// # Examples
    /// default selection     diviser=3 + segments=[1,2] + directions=[left,right]
    ///      *                                  
    ///    +---+                           +---+
    ///    |   |                          *|   |*
    ///   *|   |*                          |   |
    ///    |   |                          *|   |*
    ///    +---+                           +---+
    ///      *                               
    pub fn dock_points(&self, selection: &DockSelection, collision: &CollisionMap) -> Vec<Vec2> {
        let mut centers: Vec<Vec2> = vec![];
        let window = collision.dimensions();
        let zero = Vec2::zero();
        for &i in selection.selected.iter() {
            let offset = self.dimensions / selection.diviser * i;
            // we place every thing 1 pixel away from the border of the viewbox
            let left = self.topleft + DOWN * offset + LEFT;
            let up = self.topleft + RIGHT * offset + UP;
            let right = self.topleft + RIGHT * self.dimensions + DOWN * offset + RIGHT;
            let down = self.topleft + DOWN * self.dimensions + RIGHT * offset + DOWN;
            centers.extend(
                [left, up, right, down].into_iter().filter(|p| {
                    p.is_bigger(zero) && p.is_smaller(window) && collision.is_ok_start(p)
                }),
            );
        }
        let idx_dirs: Vec<usize> = selection.directions.iter().map(|d| *d as usize).collect();
        centers
            .into_iter()
            .enumerate()
            .filter_map(|(i, p)| {
                if idx_dirs.contains(&(i % 4)) {
                    Some(p)
                } else {
                    None
                }
            })
            .collect()
    }
}

#[cfg(test)]
mod test_viewbox {
    use super::*;
    #[test]
    fn test_directions() {
        let viewbox = Viewbox::default();
        let to_left = viewbox.topleft + LEFT;
        let to_right = viewbox.topleft + RIGHT * (viewbox.dimensions + 1);
        let to_top = viewbox.topleft + UP;
        let to_bottom = viewbox.topleft + DOWN * (viewbox.dimensions + 1);
        assert_eq!(LEFT, viewbox.direction_of(&to_left));
        assert_eq!(RIGHT, viewbox.direction_of(&to_right));
        assert_eq!(UP, viewbox.direction_of(&to_top));
        assert_eq!(DOWN, viewbox.direction_of(&to_bottom));
    }

    #[test]
    fn test_default_dock_points() {
        let collision = CollisionMap::default();
        let width = 10;
        let height = 15;
        let dimensions = Vec2::new(width, height);
        let topleft = collision.dimensions() / 2;
        let rect = Viewbox {
            dimensions,
            topleft,
        };
        let selection = DockSelection::default();
        let faces = rect.dock_points(&selection, &collision);
        println!("Rect topleft {topleft:?}, dims {dimensions:?}");
        println!("{faces:?}");
        let left = topleft + (DOWN * height / 2) + LEFT;
        let up = topleft + (RIGHT * width / 2) + UP;
        let right = topleft + (DOWN * height / 2) + RIGHT * (width + 1);
        let down = topleft + (RIGHT * width / 2) + DOWN * (height + 1);
        assert_eq!(faces[0], left);
        assert_eq!(faces[1], up);
        assert_eq!(faces[2], right);
        assert_eq!(faces[3], down);
    }

    #[test]
    fn test_dock_points_seg_right() {
        let collision = CollisionMap::default();
        let width = 10;
        let height = 15;
        let dimensions = Vec2::new(width, height);
        let topleft = collision.dimensions() / 2;
        let rect = Viewbox {
            dimensions,
            topleft,
        };
        let selection = DockSelection::faces_and_segments(3, vec![Direction::Right], vec![1]);
        let faces = rect.dock_points(&selection, &collision);
        assert_eq!(faces.len(), 1);
        let right = topleft + (DOWN * height / 3) + RIGHT * (width + 1);
        assert_eq!(faces[0], right);
    }
}
