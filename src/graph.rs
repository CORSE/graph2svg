use std::collections::{HashMap, HashSet};

use crate::constraints::DimsConstraint;
use crate::edge::{EdgeLinkPolicy, PathFinder};
use crate::{
    constraints::Constraint,
    edge::{arrow, Edge},
    error::Graph2SvgErr,
    helpers::Vec2,
    node::DEFAULT_MARGIN,
    node::{array2d, Node, NodeContent, NodeID},
    shape::Shape,
    style::*,
};
use svg::Document;

/// Global default for the minimum distance between every nodes.
pub const DEFAULT_SAFETY_DIST: usize = 40;

/// Main struct of the library used to create the graph adding nodes, edges and constraints and
/// render it to svg.
/// It also store some values to allow some overriding or extensions for styles such as [Styler].
pub struct Graph {
    root_nodes: Vec<Node>,
    root_margin: Vec2,
    edges: Vec<Edge>,
    edge_policy: EdgeLinkPolicy,
    constraints: Vec<Constraint>,
    dims_constrs: Vec<DimsConstraint>,
    local_safety: HashMap<(NodeID, NodeID), usize>,
    safety_dist: usize,
    styler: Styler,
}

impl Graph {
    /// Create a [Graph] with no [Edge] and no [Constraint] can be added latter on.
    pub fn new(root_nodes: Vec<Node>) -> Self {
        Self {
            root_nodes,
            root_margin: DEFAULT_MARGIN,
            edges: Vec::new(),
            edge_policy: EdgeLinkPolicy::default(),
            constraints: Vec::new(),
            dims_constrs: Vec::new(),
            safety_dist: DEFAULT_SAFETY_DIST,
            local_safety: HashMap::new(),
            styler: Styler::default(),
        }
    }

    /// Create a [Graph] from a more convenient format for drawing control flow graphs.
    pub fn new_cfg(
        basic_blocks: HashMap<NodeID, Vec<&str>>,
        meta_edges: Vec<(NodeID, NodeID)>,
        intra_edges: Vec<((NodeID, usize), (NodeID, usize))>,
        constraints: Vec<Constraint>,
    ) -> Self {
        let root_nodes: Vec<Node> = basic_blocks
            .into_iter()
            .map(|(id, instructions)| {
                array2d(id, instructions.into_iter().map(|e| vec![e]).collect())
            })
            .collect();
        // Diffenriate control flow edge from data dependancies.
        let class_str = "edge2";
        let marker_str = "marker2";
        let color = Color::NameStr("text");
        let mut styler = Styler::new()
            .with_style(
                class_str.to_owned(),
                Style::Edge(FillStroke {
                    stroke: color.clone(),
                    ..FillStroke::edge_default()
                }),
            )
            .add_marker(ArrowMarker {
                id: marker_str.to_owned(),
                fill: color,
                ..ArrowMarker::default()
            });

        for &edge in meta_edges.iter() {
            styler.edge_styles.insert(
                edge,
                [
                    class_str.to_owned(),
                    DEFAULT_TEXT.to_owned(),
                    marker_str.to_owned(),
                    DEFAULT_MARKER_REV.to_owned(),
                ],
            );
        }
        let mut edges: Vec<Edge> = meta_edges
            .into_iter()
            .map(|(id1, id2)| arrow(id1, id2))
            .collect();
        edges.extend(
            intra_edges
                .into_iter()
                .map(|((bb1, i1), (bb2, i2))| arrow(bb1 + i1, bb2 + i2)),
        );
        Graph {
            root_nodes,
            root_margin: DEFAULT_MARGIN,
            edges,
            edge_policy: EdgeLinkPolicy::default(),
            constraints,
            dims_constrs: Vec::new(),
            safety_dist: DEFAULT_SAFETY_DIST,
            local_safety: HashMap::new(),
            styler,
        }
    }

    /// Overide the default margin.
    pub fn with_root_margin(mut self, x: u32, y: u32) -> Self {
        self.root_margin.x = x as i32;
        self.root_margin.y = y as i32;
        self
    }

    /// Overide the default margin with tuple.
    pub fn with_root_margin_t(mut self, tup: (i32, i32)) -> Self {
        self.root_margin = Vec2 { x: tup.0, y: tup.1 };
        self
    }

    /// Set the minimum distance between every nodes.
    pub fn with_safety_dist(mut self, dist: usize) -> Self {
        self.safety_dist = dist;
        self
    }

    /// overide the minimum distance between 2 nodes.
    pub fn with_local_safety_dist(mut self, nodes: (NodeID, NodeID), dist: usize) -> Self {
        self.local_safety.insert(nodes, dist);
        self.local_safety.insert((nodes.1, nodes.0), dist);
        self
    }

    /// overide the minimum distance between pairs of nodes.
    pub fn with_locals_safety_dist(mut self, nodes_dist: HashMap<(NodeID, NodeID), usize>) -> Self {
        self.local_safety.extend(
            nodes_dist
                .into_iter()
                .flat_map(|((n1, n2), dist)| [((n1, n2), dist), ((n2, n1), dist)]),
        );
        self
    }

    /// Overide the default theme, its just an helper method to avoid building a new [Styler].
    pub fn set_theme(mut self, theme: ColorTheme) -> Self {
        self.styler.theme = theme;
        self
    }

    /// The advanced way to customize styling is to create a [Styler] and pass it to the graph with this method.
    pub fn with_styler(mut self, styler: Styler) -> Self {
        self.styler = styler;
        self
    }

    /// Add new positional constraints for [Node] placement when using the solver.
    pub fn add_constraints(mut self, constraints: Vec<Constraint>) -> Self {
        self.constraints.extend(constraints);
        self
    }

    /// Add new positional constraint for [Node] placement when using the solver.
    pub fn add_constraint(mut self, constraint: Constraint) -> Self {
        self.constraints.push(constraint);
        self
    }

    /// Add new constraint on width and height of two [Node]s, that will be solved before the call to solver.
    pub fn add_dims_constraint(mut self, constraint: DimsConstraint) -> Self {
        self.dims_constrs.push(constraint);
        self
    }

    /// Add new constraints on width and height of two [Node]s, that will be solved before the call to solver.
    pub fn add_dims_constraints(mut self, constraints: Vec<DimsConstraint>) -> Self {
        self.dims_constrs.extend(constraints);
        self
    }

    /// Add connections between nodes to render.
    pub fn add_edges(mut self, edges: Vec<Edge>) -> Self {
        self.edges.extend(edges);
        self
    }

    /// Add one connection between 2 nodes to render.
    pub fn add_edge(mut self, edge: Edge) -> Self {
        self.edges.push(edge);
        self
    }

    /// Allow to select the default docking policy with [EdgeLinkPolicy] for the pathfinding of [Edge].
    pub fn with_edge_policy(mut self, policy: EdgeLinkPolicy) -> Self {
        self.edge_policy = policy;
        self
    }

    fn get_root_id(&self) -> NodeID {
        let mut all_ids: HashSet<NodeID> = HashSet::new();
        for n in self.root_nodes.iter() {
            n.get_all_ids(&mut all_ids);
        }
        ((NodeID::MIN)..(NodeID::MAX))
            .find(|id| !all_ids.contains(id))
            .unwrap()
    }

    fn input_sanity_check(&self, root_id: NodeID) -> Result<(), Graph2SvgErr> {
        if let Some(e) = self
            .edges
            .iter()
            .find(|e| e.from_node() == root_id || e.to_node() == root_id)
        {
            return Err(Graph2SvgErr::BadIDInEdge(
                e.from_node(),
                e.to_node(),
                root_id,
            ));
        }
        for (i, c) in self.constraints.iter().enumerate() {
            for c2 in self.constraints.iter().take(i) {
                if c.contains(&root_id) {
                    return Err(Graph2SvgErr::BadIDInConstraint(c.to_string(), root_id));
                }
                if c.is_included(c2) {
                    return Err(Graph2SvgErr::EquivalentConstraints(
                        c.to_string(),
                        c2.to_string(),
                    ));
                }
            }
        }
        for n in self.root_nodes.iter() {
            n.sanity_check()?;
        }
        Ok(())
    }

    /// Compute the nodes dimensions, call the solver to get their positions and draw edges in a
    /// error handable way.
    pub fn try_to_svg(mut self, path: &str) -> Result<(), Graph2SvgErr> {
        // bootstraping root node
        let root_id = self.get_root_id();
        self.input_sanity_check(root_id)?;
        let mut root = Node::new(
            root_id,
            NodeContent::Container(self.root_nodes, self.safety_dist),
            Shape::None,
        )
        .with_margin(self.root_margin.x as u32, self.root_margin.y as u32);

        // solve Nodes width,height and x,y
        root.infer_dimensions(&mut self.styler, &self.constraints, &self.edges)?;
        root.root_solve_positions(
            self.constraints,
            &self.edges,
            self.local_safety,
            self.dims_constrs,
        )?;

        // generating svg styles
        let pathfinder = PathFinder::new(&root, self.edge_policy, &self.edges)?;
        let svg_doc = Document::new()
            .set("width", root.viewbox().dimensions.x)
            .set("height", root.viewbox().dimensions.y)
            .add(self.styler.to_style_svg()?)
            .add(self.styler.markers_svg())
            .add(root.svg_group(&self.styler))
            .add_edges(self.edges, pathfinder, &mut self.styler)?;
        svg::save(path, &svg_doc)?;
        println!("Svg successfully saved to {path}");
        Ok(())
    }

    /// Same as [Self::try_to_svg] but with a panic and an error message if an error occurs.  
    pub fn to_svg(self, path: &str) {
        match self.try_to_svg(path) {
            Ok(()) => (),
            Err(error) => panic!("Error graph2svg failed due to: {error}"),
        }
    }
}

trait SvgAddEdge {
    fn add_edges(
        self,
        edges: Vec<Edge>,
        pathfinder: PathFinder,
        styler: &mut Styler,
    ) -> Result<Self, Graph2SvgErr>
    where
        Self: Sized;
}

impl SvgAddEdge for Document {
    fn add_edges(
        mut self,
        mut edges: Vec<Edge>,
        mut pathfinder: PathFinder,
        styler: &mut Styler,
    ) -> Result<Self, Graph2SvgErr> {
        pathfinder.sort_edges(&mut edges);
        for edge in edges {
            let points = pathfinder.find(&edge)?;
            self = edge.draw_edge(self, points, styler)?;
        }
        Ok(self)
    }
}
