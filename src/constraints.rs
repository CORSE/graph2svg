use std::fmt::Display;

use crate::{error::Graph2SvgErr, helpers::Vec2, node::NodeID};

/// Useful enum to select where to put the label of an edge or what part of the nodes we must align.
#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Anchor {
    Start,
    #[default]
    Center,
    End,
    Custom(f32),
}

impl Anchor {
    /// Method to get the associated number for coordinate manipulation. Can fail if using an
    /// invalid number as `Custom` (i.e a number not in the canonical range [0. , 1.])
    /// # Examples
    /// ```rust
    /// # use graph2svg::constraints::Anchor;
    /// assert_eq!(Anchor::Center.to_float().unwrap(), 0.5);
    /// assert!(Anchor::Custom(23.4).to_float().is_err());
    /// ```
    pub fn to_float(&self) -> Result<f32, Graph2SvgErr> {
        type A = Anchor;
        match self {
            A::Start => Ok(0.0),
            A::Center => Ok(0.5),
            A::End => Ok(1.0),
            A::Custom(f) if (0.0..=1.0).contains(f) => Ok(*f),
            A::Custom(f) => Err(Graph2SvgErr::InvalidAnchor(*f)),
        }
    }
}

/// Type used to encode the constraints on [Node] placement for the solver.
#[derive(Debug, Clone, PartialEq)]
pub enum Constraint {
    /// Align n > 1 [Node]s with [Anchor] along x axis.
    AlignHorizontal(Vec<NodeID>, Anchor),

    /// Align n > 1 [Node]s with [Anchor] along y axis.
    AlignVertical(Vec<NodeID>, Anchor),

    /// Same as [Self::AlignHorizontal] + [Self::LeftToRight] in one [Constraint].
    AlignLeftToRight(Vec<NodeID>, Anchor),

    /// Same as [Self::AlignVertical] + [Self::TopToBottom] in one [Constraint].
    AlignTopToBottom(Vec<NodeID>, Anchor),

    /// Useful when you want to align horizontally n nodes with a virtual point at a given anchor of the segment between 2
    /// nodes already aligned vertically.
    /// nodes.
    /// # Examples
    /// You want to align nodes 1 and 2 with the middle of 3 and 4.
    /// ```text
    ///                        +---+
    ///                        | 3 |
    ///                        +---+
    ///                          |
    ///               +---+      |         +---+
    ///               | 1 |      o         | 2 |
    ///               +---+      |         +---+
    ///                          |
    ///                        +---+
    ///                        | 4 |
    ///                        +---+
    ///```
    AlignHorizontalSegment(Vec<NodeID>, Anchor, (NodeID, NodeID), Anchor),

    /// Useful when you want to align vertically n nodes with a virtual point at a given anchor of the segment between 2
    /// nodes already aligned horizontally.
    /// # Examples
    /// You want to align nodes 3 and 4 with the middle of 1 and 2.
    /// ```text
    ///                        +---+
    ///                        | 3 |
    ///                        +---+
    ///               +---+             +---+
    ///               | 1 |------o------| 2 |
    ///               +---+             +---+
    ///
    ///
    ///                        +---+
    ///                        | 4 |
    ///                        +---+
    ///```
    AlignVerticalSegment(Vec<NodeID>, Anchor, (NodeID, NodeID), Anchor),

    /// [Constraint] enforcing that `x_1 < x_2 < ... < x_n` (with respect to widths) for all n > 1 [Node]s.
    LeftToRight(Vec<NodeID>),

    /// [Constraint] enforcing that `y_1 < y_2 < ... < y_n` (with respect to heights) for all n > 1 [Node]s.
    TopToBottom(Vec<NodeID>),
}

impl Constraint {
    /// Test used for detect error, for example if user duplicates one constraint.
    /// # Examples
    /// ```rust
    /// # use graph2svg::constraints::{Constraint, Anchor};
    /// let align_h = Constraint::AlignHorizontal(vec![1,2], Anchor::default());
    /// let align_h_ordered = Constraint::AlignLeftToRight(vec![1,2], Anchor::default());
    /// let align_v = Constraint::AlignVertical(vec![3,4], Anchor::default());
    /// assert!(align_h.is_included(&align_h_ordered));
    /// assert!(!align_h.is_included(&align_v));
    /// ```
    pub fn is_included(&self, other: &Constraint) -> bool {
        type C = Constraint;
        match (self, other) {
            (C::AlignHorizontal(nodes, ..), C::AlignHorizontal(..))
            | (C::AlignHorizontal(nodes, ..), C::AlignLeftToRight(..))
            | (C::AlignVertical(nodes, ..), C::AlignVertical(..))
            | (C::AlignVertical(nodes, ..), C::AlignTopToBottom(..)) => {
                nodes.iter().all(|n| other.contains(n))
            }

            (C::LeftToRight(nodes, ..), C::LeftToRight(nodes_o, ..))
            | (C::LeftToRight(nodes, ..), C::AlignLeftToRight(nodes_o, ..))
            | (C::TopToBottom(nodes, ..), C::TopToBottom(nodes_o, ..))
            | (C::TopToBottom(nodes, ..), C::AlignTopToBottom(nodes_o, ..)) => {
                // test for ordered subset
                // nodes = [0, 1, 2], nodes_o = [4, 0, 1, 3, 2] => true
                // nodes = [0, 1, 2], nodes_o = [2, 0, 1, 3, 4] => false
                let mut i = 0;
                let mut i_o = 0;
                while i < nodes.len() && i_o < nodes_o.len() {
                    if nodes[i] == nodes_o[i_o] {
                        i += 1;
                    } else {
                        i_o += 1;
                    }
                }
                i == nodes.len()
            }
            _ => false,
        }
    }

    /// Simple test to see if a constraint is an align horizontal one.
    /// # Examples
    /// ```rust
    /// # use graph2svg::constraints::{Constraint, Anchor};
    /// let align_h = Constraint::AlignHorizontal(vec![1,2], Anchor::default());
    /// let align_h_ordered = Constraint::AlignLeftToRight(vec![1,2], Anchor::default());
    /// let align_v = Constraint::AlignVertical(vec![3,4], Anchor::default());
    /// assert!(align_h.is_align_h());
    /// assert!(align_h_ordered.is_align_h());
    /// assert!(!align_v.is_align_h());
    /// ```
    pub fn is_align_h(&self) -> bool {
        type C = Constraint;
        matches!(
            self,
            C::AlignHorizontalSegment(..) | C::AlignHorizontal(..) | C::AlignLeftToRight(..),
        )
    }

    /// Simple test to see if a constraint is an align vertical one.
    /// # Examples
    /// ```rust
    /// # use graph2svg::constraints::{Constraint, Anchor};
    /// let align_h = Constraint::AlignHorizontal(vec![1,2], Anchor::default());
    /// let align_h_ordered = Constraint::AlignLeftToRight(vec![1,2], Anchor::default());
    /// let align_v = Constraint::AlignVertical(vec![3,4], Anchor::default());
    /// assert!(align_v.is_align_v());
    /// assert!(!align_h.is_align_v());
    /// assert!(!align_h_ordered.is_align_v());
    /// ```
    pub fn is_align_v(&self) -> bool {
        type C = Constraint;
        matches!(
            self,
            C::AlignVerticalSegment(..) | C::AlignVertical(..) | C::AlignTopToBottom(..),
        )
    }

    fn is_order_h(&self) -> bool {
        type C = Constraint;
        matches!(self, C::LeftToRight(..) | C::AlignLeftToRight(..),)
    }

    fn is_order_v(&self) -> bool {
        type C = Constraint;
        matches!(self, C::TopToBottom(..) | C::AlignTopToBottom(..),)
    }

    /// Test if a constraint involves a node in particular.
    /// # Examples
    /// ```rust
    /// # use graph2svg::constraints::{Constraint, Anchor};
    /// let align_h = Constraint::AlignHorizontal(vec![1,2], Anchor::default());
    /// assert!(align_h.contains(&1));
    /// assert!(!align_h.contains(&3));
    /// ```
    pub fn contains(&self, node: &NodeID) -> bool {
        type C = Constraint;
        match self {
            C::AlignLeftToRight(nodes, ..)
            | C::AlignTopToBottom(nodes, ..)
            | C::AlignHorizontal(nodes, ..)
            | C::AlignVertical(nodes, ..)
            | C::AlignHorizontalSegment(nodes, ..)
            | C::AlignVerticalSegment(nodes, ..)
            | C::LeftToRight(nodes)
            | C::TopToBottom(nodes) => nodes.contains(node),
        }
    }

    fn is_ordered(&self, segment: &(NodeID, NodeID)) -> bool {
        type C = Constraint;
        match self {
            C::AlignLeftToRight(nodes, ..)
            | C::AlignTopToBottom(nodes, ..)
            | C::LeftToRight(nodes)
            | C::TopToBottom(nodes) => {
                let index_0 = nodes
                    .iter()
                    .position(|n| n == &segment.0)
                    .unwrap_or(usize::MAX);
                let index_1 = nodes
                    .iter()
                    .position(|n| n == &segment.1)
                    .unwrap_or(usize::MIN);
                index_0 < index_1
            }
            _ => false,
        }
    }

    pub fn check(&self, constraints: &[Constraint]) -> Result<(), Graph2SvgErr> {
        type C = Constraint;
        match self {
            C::AlignHorizontalSegment(_, _, segment, _) => {
                let is_aligned = constraints
                    .iter()
                    .any(|c| c.is_align_v() && c.contains(&segment.0) && c.contains(&segment.1));
                let is_ordered = constraints
                    .iter()
                    .any(|c| c.is_order_v() && c.is_ordered(segment));
                (is_aligned && is_ordered)
                    .then_some(())
                    .ok_or_else(|| Graph2SvgErr::InvalidAlignWithSegment(self.clone()))
            }
            C::AlignVerticalSegment(_, _, segment, _) => {
                let is_aligned = constraints
                    .iter()
                    .any(|c| c.is_align_h() && c.contains(&segment.0) && c.contains(&segment.1));
                let is_ordered = constraints
                    .iter()
                    .any(|c| c.is_order_h() && c.is_ordered(segment));
                (is_aligned && is_ordered)
                    .then_some(())
                    .ok_or_else(|| Graph2SvgErr::InvalidAlignWithSegment(self.clone()))
            }
            _ => Ok(()),
        }
    }
}

/// Create a default [Constraint::AlignHorizontalSegment] with both anchor centered.
pub fn align_horizontal_segment(nodes: Vec<NodeID>, segment: (NodeID, NodeID)) -> Constraint {
    Constraint::AlignHorizontalSegment(nodes, Anchor::default(), segment, Anchor::default())
}

/// Create a default [Constraint::AlignVerticalSegment] with both anchor centered.
pub fn align_vertical_segment(nodes: Vec<NodeID>, segment: (NodeID, NodeID)) -> Constraint {
    Constraint::AlignVerticalSegment(nodes, Anchor::default(), segment, Anchor::default())
}

/// Create a default [Constraint::AlignHorizontal] centered between n > 1 nodes.
pub fn align_horizontal(nodes: Vec<NodeID>) -> Constraint {
    Constraint::AlignHorizontal(nodes, Anchor::default())
}

/// Create a default [Constraint::AlignVertical] centered between n > 1 nodes.
pub fn align_vertical(nodes: Vec<NodeID>) -> Constraint {
    Constraint::AlignVertical(nodes, Anchor::default())
}

/// Same as [Constraint::LeftToRight].
pub fn order_left_right(nodes: Vec<NodeID>) -> Constraint {
    Constraint::LeftToRight(nodes)
}

/// Same as [Constraint::TopToBottom].
pub fn order_top_bottom(nodes: Vec<NodeID>) -> Constraint {
    Constraint::TopToBottom(nodes)
}

/// Same as [align_horizontal] + [order_left_right] but in one [Constraint].
pub fn align_left_to_right(nodes: Vec<NodeID>) -> Constraint {
    Constraint::AlignLeftToRight(nodes, Anchor::default())
}

/// Same as [align_vertical] + [order_top_bottom] but in one [Constraint].
pub fn align_top_to_bottom(nodes: Vec<NodeID>) -> Constraint {
    Constraint::AlignTopToBottom(nodes, Anchor::default())
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DimsConstraint {
    SameWidth(NodeID, NodeID),
    SameHeight(NodeID, NodeID),
    SameDims(NodeID, NodeID),
}

impl DimsConstraint {
    pub fn contains(&self, node: &NodeID) -> bool {
        match self {
            Self::SameDims(n1, n2) | Self::SameHeight(n1, n2) | Self::SameWidth(n1, n2) => {
                node == n1 || node == n2
            }
        }
    }
    pub fn get_nodes(&self) -> (NodeID, NodeID) {
        match self {
            Self::SameDims(n1, n2) | Self::SameHeight(n1, n2) | Self::SameWidth(n1, n2) => {
                (*n1, *n2)
            }
        }
    }

    pub(crate) fn get_dims(&self, dims1: Vec2, dims2: Vec2) -> (Vec2, Vec2) {
        match self {
            Self::SameDims(..) => {
                let x = dims1.x.max(dims2.x);
                let y = dims1.y.max(dims2.y);
                let dims = Vec2 { x, y };
                (dims, dims)
            }
            Self::SameHeight(..) => {
                let y = dims1.y.max(dims2.y);
                (Vec2 { x: dims1.x, y }, Vec2 { x: dims2.x, y })
            }
            Self::SameWidth(..) => {
                let x = dims1.x.max(dims2.x);
                (Vec2 { x, y: dims1.y }, Vec2 { x, y: dims2.y })
            }
        }
    }
}

impl Display for Constraint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}
