use std::collections::HashMap;

pub type ColorDict = HashMap<&'static str, &'static str>;

/// Available theme to change the global look of the colors used.
#[derive(Default, Debug, Clone, Copy)]
pub enum ColorTheme {
    /// A light theme with the Catppuccin colors palette (default theme).
    #[default]
    CatppuccinLatte,

    /// A dark theme with the Catppuccin colors palette.
    CatppuccinMocha,
}

/// Choose color from raw value or color name that will try to use the theme.
#[derive(Debug, Clone)]
pub enum Color {
    RGB(u8, u8, u8),
    Hex(u32),
    NameString(String),
    NameStr(&'static str),
    None,
}

impl Color {
    /// Convert the color to css value using the theme if possible (i.e for name).
    pub fn to_css(&self, theme: &ColorDict) -> String {
        type C = Color;
        match self {
            C::None => "none".to_owned(),
            C::RGB(r, g, b) => format!("rgb({r}, {g}, {b})"),
            C::Hex(bytes) => format!("{bytes:#x}").replace("0x", "#"),
            C::NameStr(name) => theme
                .get(name)
                .map(ToString::to_string)
                .unwrap_or_else(|| name.to_string()),
            C::NameString(name) => theme
                .get(name.as_str())
                .map(ToString::to_string)
                .unwrap_or_else(|| name.clone()),
        }
    }
}

impl ColorTheme {
    /// [HashMap] containings the color name to color hex code for generating css style.
    pub fn get_colors_dict(&self) -> ColorDict {
        type CS = ColorTheme;
        match self {
            CS::CatppuccinLatte => HashMap::from([
                ("rosewater", "#dc8a78"),
                ("flamingo", "#dd7878"),
                ("pink", "#ea76cb"),
                ("mauve", "#8839ef"),
                ("red", "#d20f39"),
                ("maroon", "#e64553"),
                ("peach", "#fe640b"),
                ("yellow", "#df8e1d"),
                ("green", "#40a02b"),
                ("teal", "#179299"),
                ("sky", "#04a5e5"),
                ("sapphire", "#209fb5"),
                ("blue", "#1e66f5"),
                ("lavender", "#7287fd"),
                ("text", "#4c4f69"),
                ("subtext1", "#5c5f77"),
                ("subtext0", "#6c6f85"),
                ("overlay2", "#7c7f93"),
                ("overlay1", "#8c8fa1"),
                ("overlay0", "#9ca0b0"),
                ("surface2", "#acb0be"),
                ("surface1", "#bcc0cc"),
                ("surface0", "#ccd0da"),
                ("base", "#eff1f5"),
                ("mantle", "#e6e9ef"),
                ("crust", "#dce0e8"),
            ]),
            CS::CatppuccinMocha => HashMap::from([
                ("rosewater", "#f5e0dc"),
                ("flamingo", "#f2cdcd"),
                ("pink", "#f5c2e7"),
                ("mauve", "#cba6f7"),
                ("red", "#f38ba8"),
                ("maroon", "#eba0ac"),
                ("peach", "#fab387"),
                ("yellow", "#f9e2af"),
                ("green", "#a6e3a1"),
                ("teal", "#94e2d5"),
                ("sky", "#89dceb"),
                ("sapphire", "#74c7ec"),
                ("blue", "#89b4fa"),
                ("lavender", "#b4befe"),
                ("text", "#cdd6f4"),
                ("subtext1", "#bac2de"),
                ("subtext0", "#a6adc8"),
                ("overlay2", "#9399b2"),
                ("overlay1", "#7f849c"),
                ("overlay0", "#6c7086"),
                ("surface2", "#585b70"),
                ("surface1", "#45475a"),
                ("surface0", "#313244"),
                ("base", "#1e1e2e"),
                ("mantle", "#181825"),
                ("crust", "#11111b"),
            ]),
        }
    }
}
