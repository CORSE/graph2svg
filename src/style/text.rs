use crate::style::{color_themes::*, defaults::*};

/// Style for colors of shapes, inside colors use fill properties and outside colors use stroke
/// properties.
#[derive(Debug, Clone)]
pub struct FillStroke {
    pub fill: Color,
    pub fill_opacity: f32,
    pub stroke: Color,
    pub stroke_opacity: f32,
    pub stroke_width: usize,
    pub stroke_dasharray: String,
}

impl FillStroke {
    /// Default [FillStroke] generating css style class for [crate::Edge].
    pub fn edge_default() -> Self {
        Self {
            fill: Color::None,
            fill_opacity: 0.0,
            stroke_opacity: 0.8,
            ..Default::default()
        }
    }

    /// Default [FillStroke] generating css style class for [TextStyle].
    pub fn text_default() -> Self {
        FillStroke {
            fill: TEXT_COLOR,
            fill_opacity: OPAQUE,
            stroke: Color::None,
            stroke_opacity: 0.0,
            stroke_width: 0,
            stroke_dasharray: String::default(),
        }
    }

    /// Generate the css [String] to include in the style section of the [svg::Document].
    pub fn to_css(&self, theme: &ColorDict) -> String {
        format!(
            r#"
            fill: {fill};
            fill-opacity: {fill_opacity};
            stroke: {stroke};
            stroke-opacity: {stroke_opacity};
            stroke-width: {stroke_width};
            stroke-dasharray: {stroke_dasharray};
            "#,
            fill = self.fill.to_css(theme),
            stroke = self.stroke.to_css(theme),
            fill_opacity = self.fill_opacity,
            stroke_opacity = self.stroke_opacity,
            stroke_width = self.stroke_width,
            stroke_dasharray = self.stroke_dasharray,
        )
    }
}

impl Default for FillStroke {
    /// Default colors for [crate::shape::Shape].
    fn default() -> Self {
        FillStroke {
            fill: COLOR_RECT_INSIDE,
            fill_opacity: OPAQUE,
            stroke: COLOR_RECT_BORDER,
            stroke_opacity: OPAQUE,
            stroke_width: THICKNESS_RECT,
            stroke_dasharray: String::default(),
        }
    }
}

/// Text look normal or bold.
#[derive(Debug, Clone, Default)]
pub enum FontWeight {
    #[default]
    Normal,
    Bold,
}
impl FontWeight {
    pub fn to_css(&self) -> &'static str {
        type W = FontWeight;
        match self {
            W::Normal => "normal",
            W::Bold => "bold",
        }
    }
}

/// Text look normal, italic or oblique.
#[derive(Debug, Clone, Default)]
pub enum FontStyle {
    #[default]
    Normal,
    Italic,
    Oblique,
}
impl FontStyle {
    pub fn to_css(&self) -> &'static str {
        type W = FontStyle;
        match self {
            W::Normal => "normal",
            W::Italic => "italic",
            W::Oblique => "oblique",
        }
    }
}

/// How to position the origin y of the coordinate for text.
/// # Warning
/// If you change it in any text style, it will probably ruin the style because current code expect
/// Middle to center cleanly.
#[derive(Debug, Clone, Default)]
pub enum DominantBaseline {
    Hanging,
    #[default]
    Middle,
    TextBottom,
}

impl DominantBaseline {
    pub fn to_css(&self) -> &'static str {
        type B = DominantBaseline;
        match self {
            B::Middle => "middle",
            B::TextBottom => "text-bottom",
            B::Hanging => "hanging",
        }
    }
}
/// How to position the origin  x of the coordinate for text.
/// # Warning
/// If you change it in any text style, it will probably ruin the style because current code expect
/// Middle to center cleanly.
#[derive(Debug, Clone, Default)]
pub enum TextAnchor {
    Start,
    #[default]
    Middle,
    End,
}

impl TextAnchor {
    pub fn to_css(&self) -> &'static str {
        type A = TextAnchor;
        match self {
            A::Start => "start",
            A::Middle => "middle",
            A::End => "end",
        }
    }
}

/// Gather all styling important parameters for customizing text look.
#[derive(Debug, Clone)]
pub struct TextStyle {
    pub font: String,
    pub font_size: usize,
    pub font_weight: FontWeight,
    pub font_style: FontStyle,
    pub text_anchor: TextAnchor,
    pub dominant_baseline: DominantBaseline,
    pub colors: FillStroke,
}

impl TextStyle {
    pub fn to_css(&self, theme: &ColorDict) -> String {
        format!(
            r#"
            font: {font};
            font-size: {font_size}px;
            font-weight: {font_weight};
            font-style: {font_style};
            text-anchor: {text_anchor};
            dominant-baseline: {dominant_baseline};
            {colors}
            "#,
            font = self.font,
            font_size = self.font_size,
            font_weight = self.font_weight.to_css(),
            font_style = self.font_style.to_css(),
            colors = self.colors.to_css(theme),
            text_anchor = self.text_anchor.to_css(),
            dominant_baseline = self.dominant_baseline.to_css()
        )
    }
}

impl Default for TextStyle {
    fn default() -> Self {
        Self {
            font: DEFAULT_FONT.to_owned(),
            font_size: 12,
            font_weight: FontWeight::default(),
            font_style: FontStyle::default(),
            dominant_baseline: DominantBaseline::default(),
            text_anchor: TextAnchor::default(),
            colors: FillStroke::text_default(),
        }
    }
}
