use crate::style::color_themes::Color;

pub const DEFAULT_FONT: &str = "JetBrains Mono";
pub const TEXT_COLOR: Color = Color::NameStr("text");
pub const BACKGROUND_COLOR: Color = Color::NameStr("base");
pub const COLOR_EDGE: Color = Color::NameStr("overlay0");
pub const COLOR_RECT_BORDER: Color = COLOR_EDGE;
pub const COLOR_RECT_INSIDE: Color = Color::NameStr("surface0");
pub const THICKNESS_RECT: usize = 1;
pub const OPAQUE: f32 = 1.0;
pub const DEFAULT_CIRCLE: &str = "graph2svg_circle";
pub const DEFAULT_RECT: &str = "graph2svg_rect";
pub const DEFAULT_TEXT: &str = "graph2svg_text";
pub const DEFAULT_EDGE: &str = "graph2svg_edge";
pub const DEFAULT_MARKER: &str = "graph2svg_marker";
pub const DEFAULT_MARKER_REV: &str = "graph2svg_marker_rev";
