mod color_themes;
mod defaults;
mod text;

use font_kit::font::Font;
use std::collections::HashMap;
use std::fmt::Write;
use svg::node::element::Definitions;
use svg::node::element::Marker;
use svg::node::element::Path;

pub use color_themes::*;
pub use defaults::*;
pub use text::*;

use crate::error::Graph2SvgErr;
use crate::node::NodeID;

/// Struct to change color theme, default styles or locally overwrite some styles for a particular [crate::Node] or [crate::Edge].
#[derive(Debug, Clone)]
pub struct Styler {
    pub(crate) theme: ColorTheme,
    css_classes: HashMap<String, Style>,
    markers: Vec<ArrowMarker>,
    pub(crate) fonts_cache: HashMap<String, Font>,
    /// key is node ,value is (shape_style_class, text_style_class)
    pub(crate) node_styles: HashMap<NodeID, (String, String)>,
    /// key is (from, to) ,value is (shape_style_class, label_style_class, marker_id, marker_id_rev)
    pub(crate) edge_styles: HashMap<(NodeID, NodeID), [String; 4]>,
}

impl Styler {
    /// Create `<defs><marker>..` svg data
    pub(crate) fn markers_svg(&self) -> Definitions {
        let theme = self.theme.get_colors_dict();
        self.markers.iter().fold(Definitions::new(), |acc, marker| {
            acc.add(marker.to_svg(&theme))
        })
    }
    /// Create `<style>` svg data
    pub(crate) fn to_style_svg(&self) -> Result<svg::node::element::Style, Graph2SvgErr> {
        let mut css = String::new();
        let theme = self.theme.get_colors_dict();
        for (css_class, style) in self.css_classes.iter() {
            let css_attrs = style.to_css(&theme);
            let prefix = if css_class == "svg" { "" } else { "." };
            writeln!(&mut css, "{prefix}{css_class} {{{css_attrs}}}")?;
        }
        Ok(svg::node::element::Style::new(css))
    }
    /// Get the text style class of a node.
    pub(crate) fn node_text_class(&self, node: NodeID) -> &str {
        self.node_styles
            .get(&node)
            .map(|(_, text)| text.as_str())
            .unwrap_or(DEFAULT_TEXT)
    }
    /// Get the [TextStyle] of a node.
    pub(crate) fn node_text_style(&self, node: NodeID) -> Result<&TextStyle, Graph2SvgErr> {
        let class = self.node_text_class(node);
        self.css_classes
            .get(class)
            .map(|style| {
                if let Style::Text(text_style) = style {
                    Ok(text_style)
                } else {
                    Err(Graph2SvgErr::BadTextStyle(class.to_string()))
                }
            })
            .ok_or_else(|| Graph2SvgErr::UnknownCssClass(class.to_string()))?
    }

    /// Get the [TextStyle] of an edge.
    pub(crate) fn edge_text_style(
        &self,
        from: NodeID,
        to: NodeID,
    ) -> Result<&TextStyle, Graph2SvgErr> {
        let class = self
            .edge_styles
            .get(&(from, to))
            .map(|classes| classes[1].as_str())
            .unwrap_or(DEFAULT_TEXT);
        self.css_classes
            .get(class)
            .map(|style| {
                if let Style::Text(text_style) = style {
                    Ok(text_style)
                } else {
                    Err(Graph2SvgErr::BadTextStyle(class.to_string()))
                }
            })
            .ok_or_else(|| Graph2SvgErr::UnknownCssClass(class.to_string()))?
    }

    /// Get the css class names and marker id for an edge.
    pub(crate) fn edge_classes(&self, from: NodeID, to: NodeID) -> [String; 4] {
        self.edge_styles
            .get(&(from, to))
            .map(Clone::clone)
            .unwrap_or([
                DEFAULT_EDGE.to_owned(),
                DEFAULT_TEXT.to_owned(),
                DEFAULT_MARKER.to_owned(),
                DEFAULT_MARKER_REV.to_owned(),
            ])
    }
    /// Create a new style using [Default] values.
    pub fn new() -> Self {
        Self::default()
    }
    /// Default with a choosen theme.
    pub fn new_theme(theme: ColorTheme) -> Self {
        Self {
            theme,
            ..Default::default()
        }
    }
    /// Add a new css class with its styling attributes or overides one default.
    pub fn with_style(mut self, css_class: String, style: Style) -> Self {
        self.css_classes.insert(css_class, style);
        self
    }
    /// Add new classes and their styling attributes or overrides the defaults.
    pub fn with_styles(mut self, styles: HashMap<String, Style>) -> Self {
        self.css_classes.extend(styles);
        self
    }
    /// Local overides the css class of a node **shape** style.
    pub fn style_node_shape(mut self, node: NodeID, css_class: String) -> Self {
        self.node_styles
            .insert(node, (css_class, DEFAULT_TEXT.to_owned()));
        self
    }
    /// Local overides the css class of a node **text** style.
    pub fn style_node_text(mut self, node: NodeID, css_class: String) -> Self {
        self.node_styles
            .insert(node, (DEFAULT_RECT.to_owned(), css_class));
        self
    }
    /// Local overides the css classes for shape and text of a set of nodes.
    pub fn style_nodes(mut self, shape_texts: HashMap<NodeID, (String, String)>) -> Self {
        self.node_styles.extend(shape_texts);
        self
    }
    /// Local overides the css class of an edge **shape and marker** style.
    pub fn style_edge_shape(
        mut self,
        from: NodeID,
        to: NodeID,
        css_class: String,
        marker_id: String,
        marker_id_rev: String,
    ) -> Self {
        self.edge_styles.insert(
            (from, to),
            [css_class, DEFAULT_TEXT.to_owned(), marker_id, marker_id_rev],
        );
        self
    }
    /// Local overides the css class of an edge **label text** style.
    pub fn style_edge_label(mut self, from: NodeID, to: NodeID, css_class: String) -> Self {
        self.edge_styles.insert(
            (from, to),
            [
                DEFAULT_EDGE.to_owned(),
                css_class,
                DEFAULT_MARKER.to_owned(),
                DEFAULT_MARKER_REV.to_owned(),
            ],
        );
        self
    }
    /// Local overides the css classes for shape, label and marker of a set of edges.
    pub fn style_edges(
        mut self,
        shape_text_markers: HashMap<(NodeID, NodeID), [String; 4]>,
    ) -> Self {
        self.edge_styles.extend(shape_text_markers);
        self
    }
    /// Add an [ArrowMarker] as a `<marker>` to the svg.
    pub fn add_marker(mut self, marker: ArrowMarker) -> Self {
        self.markers.push(marker);
        self
    }
    /// Add list of [ArrowMarker] as multiple `<marker>` to the svg.
    pub fn add_markers(mut self, markers: Vec<ArrowMarker>) -> Self {
        self.markers.extend(markers);
        self
    }
}

impl Default for Styler {
    fn default() -> Self {
        Self {
            theme: ColorTheme::default(),
            css_classes: HashMap::from([
                (DEFAULT_RECT.to_owned(), Style::Rect(FillStroke::default())),
                (
                    DEFAULT_CIRCLE.to_owned(),
                    Style::Circle(FillStroke::default()),
                ),
                (DEFAULT_TEXT.to_owned(), Style::Text(TextStyle::default())),
                (
                    DEFAULT_EDGE.to_owned(),
                    Style::Edge(FillStroke::edge_default()),
                ),
                (
                    "svg".to_owned(),
                    Style::Svg {
                        background: BACKGROUND_COLOR,
                    },
                ),
            ]),
            markers: vec![ArrowMarker::default(), ArrowMarker::default_rev()],
            fonts_cache: HashMap::new(),
            node_styles: HashMap::new(),
            edge_styles: HashMap::new(),
        }
    }
}

/// How to define markers.
#[derive(Debug, Clone)]
pub struct ArrowMarker {
    pub id: String,
    pub width: u8,
    pub height: u8,
    /// New origin x of the viewbox of the triangle to draw.
    pub ref_x: u8,
    /// New origin y of the viewbox of the triangle to draw.
    pub ref_y: u8,
    /// Color of the marker
    pub fill: Color,
    pub is_rev: bool,
}

impl ArrowMarker {
    pub fn default_rev() -> Self {
        ArrowMarker {
            id: DEFAULT_MARKER_REV.to_owned(),
            ref_x: 1,
            is_rev: true,
            ..Default::default()
        }
    }
    pub fn to_svg(&self, theme: &ColorDict) -> Marker {
        let (w, h) = (self.width, self.height);
        let hh = h / 2;
        let path_data = if self.is_rev {
            format!("M {w} 0 0 {hh} {w} {h} z")
        } else {
            format!("M 0 0 L {w} {hh} L 0 {h} z")
        };
        let path = Path::new()
            .set("d", path_data)
            .set("fill", self.fill.to_css(theme));
        Marker::new()
            .set("id", self.id.as_str())
            .set("viewbox", format!("0 0 {} {}", self.width, self.height))
            .set("refX", self.ref_x)
            .set("refY", self.ref_y)
            .set("markerWidth", self.width)
            .set("markerHeight", self.height)
            .set("orient", "auto")
            .add(path)
    }
}

impl Default for ArrowMarker {
    fn default() -> Self {
        Self {
            id: DEFAULT_MARKER.to_owned(),
            width: 6,
            height: 6,
            ref_x: 5,
            ref_y: 3,
            fill: COLOR_EDGE,
            is_rev: false,
        }
    }
}

/// Style for the global svg.
#[derive(Debug, Clone)]
pub struct SvgStyle {
    pub background: Color,
}

/// The available styles to overides.
#[derive(Debug, Clone)]
pub enum Style {
    Text(TextStyle),
    Rect(FillStroke),
    Circle(FillStroke),
    Edge(FillStroke),
    Svg { background: Color },
}

impl Style {
    pub fn to_css(&self, theme: &ColorDict) -> String {
        type S = Style;
        match self {
            S::Text(style) => style.to_css(theme),
            S::Rect(style) | S::Circle(style) | S::Edge(style) => style.to_css(theme),
            S::Svg { background } => {
                format!("background: {};", background.to_css(theme))
            }
        }
    }
}
