extern crate graph2svg;

use graph2svg::prelude::*;
use graph2svg::style::{Color, FillStroke, Style, Styler};

fn main() {
    let redcell = "mycell";
    let (id_header, id_array) = (0, 1);
    let (n_rows, n_cols) = (4, 4);
    let (choosen_i, choosen_j) = (2, 1);

    let mut array: Vec<Vec<Node>> = vec![];
    let mut id = id_array + 1;
    for _ in 0..n_rows {
        let mut line = vec![];
        for _ in 0..n_cols {
            let cell = text_boxed(id, "lul").with_shape(shape_rect_sharp());
            line.push(cell);
            id += 1;
        }
        array.push(line);
    }
    let styler = Styler::new_theme(ColorTheme::CatppuccinMocha)
        .with_style(
            redcell.to_owned(),
            Style::Rect(FillStroke {
                fill: Color::NameStr("red"),
                ..Default::default()
            }),
        )
        .style_node_shape(array[choosen_i][choosen_j].id(), redcell.to_owned());
    let array =
        Node::new(id_array, NodeContent::Array2D(array, (0, 0)), Shape::None).with_margin(0, 0);

    Graph::new(vec![
        text_boxed(id_header, "Hello Very Very Very Large World!").with_shape(shape_rect_sharp()),
        array,
    ])
    .add_constraint(align_top_to_bottom(vec![id_header, id_array]))
    .add_dims_constraint(DimsConstraint::SameWidth(id_header, id_array))
    .with_local_safety_dist((id_header, id_array), 0) // allow to glue array1 and header
    .with_styler(styler)
    .to_svg("/tmp/test.svg")
}
