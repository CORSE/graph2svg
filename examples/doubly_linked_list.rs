extern crate graph2svg;

use graph2svg::prelude::*;

fn main() {
    const N: usize = 6;
    const ID_BASE: usize = 100;
    let content = vec![vec!["a"; N]];
    let up_right = DockSelection::faces_and_segments(3, vec![Direction::Right], vec![1]);
    let up_left = DockSelection::faces_and_segments(3, vec![Direction::Left], vec![1]);
    let down_right = DockSelection::faces_and_segments(3, vec![Direction::Right], vec![2]);
    let down_left = DockSelection::faces_and_segments(3, vec![Direction::Left], vec![2]);
    let edges: Vec<Edge> = ((ID_BASE + 1)..(ID_BASE + N))
        .flat_map(|id| {
            [
                arrow(id, id + 1)
                    .with_from_dock(up_right.clone())
                    .with_to_dock(up_left.clone()),
                arrow(id + 1, id)
                    .with_from_dock(down_left.clone())
                    .with_to_dock(down_right.clone()),
            ]
        })
        .collect();
    Graph::new(vec![array2d_cell_margin(ID_BASE, content, 30, 0)])
        .add_edges(edges)
        .to_svg("/tmp/test.svg")
}
