extern crate graph2svg;

use graph2svg::prelude::*;

fn main() {
    let id_header = 100;
    let id_array0 = 0;
    let array0 = vec![
        vec![0],
        vec![1, 2],
        vec![3, 4, 5],
        vec![6, 7, 8, 9], // row 0
    ];
    // be careful with indices because simple_array2d assume that id of array cells
    // are base_id + index (in a row major matrix way)
    let n_ids: usize = array0.iter().map(|v| v.len()).sum();
    let id_array1 = id_array0 + 1 + n_ids;
    let array1 = vec![vec!["lul"; 3]; 2];
    let header1 = text_boxed(id_header, "Header").with_shape(shape_rect_sharp());
    Graph::new(vec![
        array2d(id_array0, array0),
        array2d(id_array1, array1).with_margin(0, 0),
        header1,
    ])
    .with_local_safety_dist((id_header, id_array1), 0) // allow to glue array1 and header
    .add_constraint(align_top_to_bottom(vec![id_header, id_array1]))
    .add_dims_constraint(DimsConstraint::SameWidth(id_header, id_array1))
    .to_svg("/tmp/test.svg")
}
