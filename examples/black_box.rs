extern crate graph2svg;

use graph2svg::prelude::*;

fn main() {
    Graph::new(vec![
        text_boxed(0, "Black Box"),
        text_boxed(1, "Input 1"),
        text_boxed(2, "Input 2"),
        text_boxed(3, "Input 3"),
        text_boxed(11, "Output 1"),
        text_boxed(12, "Output 2"),
        text_boxed(13, "Output 3"),
    ])
    .add_constraints(vec![
        align_horizontal(vec![1, 11]),
        align_horizontal(vec![3, 13]),
        align_top_to_bottom(vec![1, 2, 3]),
        align_top_to_bottom(vec![11, 12, 13]),
        align_left_to_right(vec![2, 0, 12]),
    ])
    .add_edges(vec![
        arrow(1, 0),
        arrow(2, 0),
        arrow(3, 0),
        arrow(0, 11),
        arrow(0, 12),
        arrow(0, 13),
    ])
    .with_edge_policy(EdgeLinkPolicy::FactorizeIO)
    .to_svg("/tmp/test.svg")
}
