extern crate graph2svg;

use graph2svg::prelude::*;
use std::collections::HashMap;

fn main() {
    let basic_blocks: HashMap<NodeID, Vec<&str>> = HashMap::from([
        (
            0x1A70,
            vec![
                "push rbp",
                "vmovsd xmm3, xmm0, xmm0",
                "mov r8d, 0x34",
                "mov rbp, rsp",
                "and rsp, 0xffffffffffffffe0",
                "mov dword ptr [rsp - 4], edi",
                "mov dword ptr [rsp - 8], esi",
                "mov r11, qword ptr [rbp + 0x18]",
                "mov r10, qword ptr [rbp + 0x20]",
                "mov r9, qword ptr [rbp + 0x10]",
                "nop word ptr cs:[rax + rax]",
            ],
        ),
        (
            0x1AA0,
            vec![
                "movsxd rdi, dword ptr [rsp - 4]",
                "movsxd rsi, dword ptr [rsp - 8]",
                "imul rax, rdi, 0x2260",
                "imul rdi, rdi, 0x1c20",
                "add rax, r11",
                "vmulsd xmm2, xmm3, qword ptr [rax + rsi*8]",
                "imul rsi, rsi, 0x1c20",
                "lea rdx, [r9 + rdi]",
                "xor eax, eax",
                "lea rcx, [r10 + rsi]",
                "vbroadcastsd ymm1, xmm2",
                "nop word ptr cs:[rax + rax]",
            ],
        ),
        (
            0x1AE0,
            vec![
                "vmovupd ymm0, ymmword ptr [rcx + rax]",
                "vfmadd213pd ymm0, ymm1, ymmword ptr [rdx + rax]",
                "vmovupd ymmword ptr [rdx + rax], ymm0",
                "add rax, 0x20",
                "cmp rax, 0x5e0",
                "jne 0x1ae0",
            ],
        ),
        (
            0x1AFC,
            vec![
                "lea rax, [r9 + rdi + 0x5e0]",
                "vmovupd xmm4, xmmword ptr [rax]",
                "vmovddup xmm0, xmm2",
                "vfmadd132pd xmm0, xmm4, xmmword ptr [r10 + rsi + 0x5e0]",
                "vmovupd xmmword ptr [rax], xmm0",
                "dec r8",
                "jne 0x1aa0",
            ],
        ),
        (0x1B1F, vec!["vzeroupper ", "leave ", "ret "]),
    ]);
    let control_flow = vec![
        (0x1A70, 0x1AA0),
        (0x1AA0, 0x1AE0),
        (0x1AE0, 0x1AFC),
        (0x1AE0, 0x1AE0),
        (0x1AFC, 0x1B1F),
        (0x1AFC, 0x1AA0),
    ];
    let data_dependancies = vec![((0x1AA0, 1), (0x1AA0, 6)), ((0x1AA0, 2), (0x1AA0, 7))];
    let constraints = vec![];
    Graph::new_cfg(basic_blocks, control_flow, data_dependancies, constraints)
        .to_svg("/tmp/test.svg")
}
