extern crate graph2svg;

use graph2svg::prelude::*;

fn main() {
    Graph::new(vec![
        text_boxed(1, "Hello World!"),
        text_boxed(2, "Hi"),
        text_boxed(3, "So long"),
        text_boxed(4, "Bye Bye"),
    ])
    .add_constraints(vec![
        align_left_to_right(vec![1, 2]),
        align_top_to_bottom(vec![3, 4]),
        align_vertical_segment(vec![3, 4], (1, 2)),
        align_horizontal_segment(vec![1, 2], (3, 4)),
    ])
    .add_edges(vec![edge(1, 2), edge(3, 4)])
    .set_theme(ColorTheme::CatppuccinMocha)
    .to_svg("/tmp/test.svg")
}
