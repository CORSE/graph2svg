extern crate graph2svg;

use graph2svg::prelude::*;

fn main() {
    let subgraph = Node::new(
        10,
        NodeContent::Container(
            vec![
                vertical_text_boxed(11, "Next 1"),
                text_circled(12, "Next 2"),
            ],
            DEFAULT_SAFETY_DIST,
        ),
        shape_rect_sharp(),
    );
    let subgraph2 = Node::new(
        20,
        NodeContent::Container(
            vec![text_boxed(21, "Next 3"), text_circled(22, "Next 4")],
            DEFAULT_SAFETY_DIST,
        ),
        Shape::Circle,
    );
    Graph::new(vec![subgraph, subgraph2])
        .add_constraints(vec![align_top_to_bottom(vec![11, 12, 21, 22])])
        .add_edges(vec![
            arrow(11, 12).with_vertical_label("Coucou".to_string()),
            arrow(12, 21),
            arrow_bi(21, 22).with_label("Salut".to_string()),
        ])
        .to_svg("/tmp/test.svg")
}
